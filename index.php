<?php 
  include 'sys/db.php';
  include 'sys/config.php';  
  error_reporting(0);
  $c = new Config();
  $db = new Database();
  // $banner = $db->query("SELECT * FROM banner")->rowObject(); // Gey All Banner
  $about_us = $db->query("SELECT * FROM about_us_home")->rowObject(); // Gey All About US
  $section_home =  $db->query("SELECT * FROM section_home")->rowObject(); // Section Home
  $useCase1 =  $db->query("SELECT * FROM use_case_1")->rowObject(); // Use Case 1
  $useCase2 =  $db->query("SELECT * FROM use_case_2")->rowObject(); // Use Case 2
  $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  print_r($actual_link);
  $banner      = $db->query("SELECT * FROM banner WHERE url_endpoint = '$actual_link' ")->rowObject(); // Effect Group
?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="ALL IN ONE INTEL DATA">
    <meta name="author" content="Effect Group">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/eg-icon.png">

    <title>Effect Group | Business Intelligence Operations Base</title>

    <!-- vendor css -->
    <link href="lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/dashforge.landing.css">
    <!-- <link rel="stylesheet" href="assets/css/jquery.background-video.css"> -->
    <link rel="stylesheet" href="popup/videopopup.css">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css"> -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <style>
      html {
        overflow-x: hidden;
      }

      #home {
        height: 100vh;
        margin-top: 0px;
        background-color: black;
      }

      #afterHome {

        position: relative; 
        background-color: white; 
        padding-top: 100px;
        padding-bottom: 0px;

      }

      #myVideo {
        /* padding-top: 0px; */
        position: absolute;
        top: 0;
        /* margin-top: -40%; */
        right: 0;
        /* bottom: 0; */
        min-width: 100%;
        min-height: 100%;
        height: 100vh;
        object-fit: fill;
        opacity: 0.5;
      }
      .screenshot {
        cursor: pointer;
      }

      .hovereds {
        border-left: 5px solid #001737;
      }
      .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: #141C2B;
      }

      .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
      }

      .fitur {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
      }

      .callback {
        font-size: 50px;
        color: #015de1;
      }
      .bg-dark {
        background-color: black!important;
      }
      .bg-purple {

        background-image: url("img/network-left.png"),  url("img/network.png"); /* The image used */
        background-color:white; /* Used if the image is unavailable */
        background-position: left, right; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: 30%, 30%; /* Resize the background image to cover the entire container */
      }
      .badge-primary {
        background-color: #7757ff;
      }
      .check {
        background-color: #7757ff;
        padding: 5px 10px;
        border-radius: 3px;
        color: white;
      }
      .use-case {
        cursor: pointer;
      }

      .nav-item.active {
        color: #7757ff!important;
        letter-spacing: 3px;
        text-transform: uppercase;
      }
      .bordered {
        border: 3px solid #7757ff;
      }

      .bordered-01 {
        border: 2px solid #001737;
      }
      .absoluted {
        position: absolute;
        width: 100%;
      }
      .header-one {
        width: 40%;
        position: absolute;
        top: 23%;
        right: 5%;
        z-index: 3;
        /* transform: rotate(30deg); */
      }
      .header-two {
        width: 30%;
        position: absolute;
        top: 8%;
        right: 25%;
        z-index: 1;
        /* transform: rotate(30deg); */
      }
      .header-three {
        width: 50%;
        position: absolute;
        top: 2%;
        right: 0%;
        z-index: 2;
        opacity: 0.1;
        /* transform: rotate(30deg); */
      }

      .headers-one {
        width: 50%;
        position: absolute;
        top: 2%;
        left: 0%;
        z-index: 2;
        opacity: 0.1;
        /* transform: rotate(30deg); */
      }

      .header-ss-1 {
        /* width: 50%; */
        position: absolute;
        top: -10%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      .header-four {
        opacity: 0.2;
        width: 45%;
        position: absolute;
        top: 8%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }
      .header-five {
        opacity: 0.1;
        width: 65%;
        position: absolute;
        top: -30%;
        right: -13%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      .header-six {
       width: 80%;
       margin-top: -50px;
       margin-bottom: -50px;
      }

      .text-nav {
        color: white!important;
      }

      .desktop {
        display: block;
      }

      .handphone {
        display: none;
      }

      .cookiealert {
        position: fixed;
        bottom: 0; 
        left: 0; 
        width: 30%; 
        z-index: 9998
      }

      @media (min-width: 320px) and (max-width: 480px) {
      .header-five {
        display: none;
      }

      .cookiealert {
        position: fixed;
        bottom: 0; 
        left: 0; 
        width: 100%; 
        z-index: 9998
      }
      
      .fitur {
        position: relative;
      }

      #afterHome {

        position: relative; 
        background-color: white; 
        padding-top: 50px;
        padding-bottom: 0px;
        margin-bottom: 0px;

      }


      #home {
        height: 100%;
        margin-top: 20px;
        background-color: black;
        padding-top: 50px;
        padding-bottom: 50px;
      }

      #myVideo {
        /* padding-top: 0px; */
        position: absolute;
        top: 0;
        /* margin-top: -40%; */
        right: 0;
        /* bottom: 0; */
        min-width: 100%;
        min-height: 100%;
        /* height: 100vh; */
        /* object-fit: fill; */
        opacity: 0.5;
      }

      .desktop {
        display: none;
      }

      .handphone {
        display: block;
      }

      .text-nav {
        color: #001737!important;
      }
      .callback {
        font-size:35px;
      }
      .bg-purple {
        background-image: url(""); /* The image used */
        background-color:white; /* Used if the image is unavailable */
      }
      .header-four {
        display: none!important;
      }

      .header-six {
        display: none!important;
      }

      .use-case {
        height: 100px;
        border: 3px solid #7757ff;
      }
  
        .header-one {
        width: 75%;
        position: absolute;
        top: 15%;
        left: 20%;
        z-index: 3;
        /* transform: rotate(30deg); */
      }
      .header-two {
        width: 50%;
        position: absolute;
        top: 10%;
        left: 8%;
        z-index: 1;
        /* transform: rotate(30deg); */
      }
      .header-three {
        width: 80%;
        position: absolute;
        top: 8%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      
        
      }
    </style>

  </head>
  <body class="home-body pt-3">
    <div class="preloader">
      <div class="loading">
        <img src="https://dashboard.effect.group/img/loaders.gif" width="300">
      </div>
    </div>

    <?php include "layout/header_navigation.php" ?>  

    <div id="home" class="home-slider sect">
      <!-- <img src="img/headers-1.png" class="header-one">
      <img src="img/headers-2.png" class="header-two"> -->
      <img src="img/network.png" class="header-three">
      <img src="img/network-left.png" class="headers-one">
      <div id="homeDiv" class="row justify-content-center" style="z-index: 999!important">
        <div class="col-md-4 text-center">
          <h1 class="tx-bold tx-spacing-2 mb-3 text-white header-text callback"><?php echo $banner[0]->nama_banner ?></span></h1>
          <!-- <h1 class="tx-bold tx-spacing-2 mb-3 text-white header-text callback"><?= $key->nama_banner ?> <span class="tx-light tx-white"><?= $key->nama_banner2 ?></span></h1> -->
          <p class="tx-spacing-1 mb-5 text-white"><?php echo $banner[0]->description ?></p>
          <div class="text-center">
            <a id="video-trigger" href="javascript:void(0)" class="btn btn-light text-primary btn-uppercase"><i data-feather="play" class="mr-2"></i> Watch Video</a>
  
              <a href="https://dashboard.effect.group/registration" target="_blank" class="btn btn-link text-white ml-2">Get Access Now <i data-feather="arrow-right" class="ml-2"></i></a>
          </div>
         

        </div>
        
        
      </div>

      <video autoplay muted loop id="myVideo">
        <source src="img/headers.mp4" type="video/mp4">
      </video>
      
    </div><!-- home-slider -->

    <div id="afterHome">
      <img src="img/network.png" class="header-four">
      <div class="container-fluid pl-md-5">
          <div class="row row-xs">
            <?php $no = 1; foreach ($section_home as $key): ?>
              <div class="col-md-5">

                <div class="card card-body shadow mb-2 screenshot hovereds" data-target="<?= $key->id_home ?>" data-aos="fade-down" data-aos-delay="100">
                  <div class="row">
                    <div class="col-2">
                      <img src="<?= $key->icons_home ?>" class="wd-50 img-fluid" style="position:absolute; top: 50%; left: 50%;  transform: translate(-50%, -50%);">
                    </div>
                    <div class="col-10" >
                      <h2 class="tx-semibold"><?= $key->title_home ?></h2>
                      <small><?= $key->description_home ?>
                      </small>
                    </div>
                  </div>
                </div>

                <!-- <div class="card card-body shadow mb-3 screenshot" data-target="ssAttack" data-aos="fade-down" data-aos-delay="400">

                  <div class="row">
                    <div class="col-2">
                      <img src="img/trojan-horse.svg" class="wd-50 img-fluid" style="position:absolute; top: 50%; left: 50%;  transform: translate(-50%, -50%);">
                    </div>
                    <div class="col-10">
                      <h2 class="tx-semibold">Attack or Train</h2>
                      <small>Involve your Team in Social Engineering and IT Infrastructure
                        knowledge & Attack Techniques, allowing an active defence of Corporate Assets,
                        making your employees aware of the dangers of the Net</p>
                      </small>
                    </div>                 
                  
                  </div>

                </div> -->

              </div>
              <div class="col-md-7 p-0">
                <!-- <img id="<?= $key->id_home ?>" src="<?= $key->image_home ?>" class="img-fluid fitur ssResearch" style="display: none; z-index: 5!important" > -->
                <img id="ssDefend" src="img/research-ss2.png" class="img-fluid fitur ssDefend" style="display: none; z-index: 5!important">
                <img id="ssAttack" src="img/research-ss3.png" class="img-fluid fitur ssAttack" style="display: none; z-index: 5!important">
              </div>
            <?php endforeach ?>
          </div>
      </div>
    </div>

    <div id="aboutUs" class="bg-primary-dark sect" style="padding-top: 80px; padding-bottom: 80px;position: relative;margin-top: 50px">
      
      <div class="container">
          <div class="row">
            <?php foreach ($about_us as $about): ?>
              <div class="col-md-5 mb-md-0 mb-4" >
                <h1 class="tx-white tx-spacing-2 tx-semibold mb-3 mt-3" style="font-size: 55px;!important; "data-aos="zoom-in" ><?php echo ($about->title_about) ?> <span class="tx-info">.</span></h1>
                <p class="text-white tx-spacing-1 mr-md-5 tx-md-14" data-aos="zoom-in" data-aos-delay="200">
                  <?php echo ($about->description) ?>
                </p>

                <!-- <p class="text-white tx-spacing-1 mr-md-5 tx-md-14" data-aos="zoom-in" data-aos-delay="200">
                  The amount and dispersion of data running on the Web is so overwhelming that tools are
                  required to obtain fast and reliable information, reducing risks and simplifying collection
                  processes, to be transformed into knowledge
                  Our team, multidisciplinary and with a broad background in Cyber Research, provides the
                  necessary knowledge for our Technical Department to implement continuous improvements
                  that maintain our Service Up-To-Date.
                </p> -->
              </div>  
            <?php endforeach ?>
            
            <div class="col-md-7">
              
              <button id="choosefeatures" class="btn btn-brand-01 text-white choose" data-target="features" ><i data-feather="star" class="mr-2"></i> Features</button> 
              <button id="chooseBenefits" class="btn btn-link text-white choose" data-target="benefits"><i data-feather="award" class="mr-2"></i> Benefits</button>
              <div id="features" class="card card-body mt-2 choosen" >
                <ul>
                  <li>Fast People Access Data (Personal, Professional & Social). More than 3 Billion + profiles available in our Private Datasets with over 170 data points</li>   
                  <li>Deep explore of Companies & Officers Wordwide</li>
                  <li>Prevent your Organization from Data Breaches, with our most reliable Leak Sources</li>
                  <li>People & Corporates Money Laundering discover</li>
                  <li>Prevent Risk Business: People and Organization Anti Fraud & Corruption activities Search Engines</li>
                  <li>Discover multiple target files and documents on the CDN Amazon Web Service</li>
                  <li>Image Analysis Toolset (Image AI Prediction, Sentiment, Image People Search, Logo and Face Recognition)</li>
                  <!-- <li>Attack & Defend: Run Social Engineering & Databases Exploting Tools. Know yourenemy. Know your weaknesses</li> -->
                  <li>Research Social Networks as never before (Coming Soon)</li>
                  <li>One Click - One Space: Acces all the Business Intelligence in your favourite web browser, in a Only Space Operations Based System</li>
                  <li>Web Based User Interface. Saas Busines Intelligence</li>
                  <!-- <li>Android APP Available</li> -->
                  <li>Signal Intelligence Chatbot: Generates Intel Data Reports directly from your Mobile Phone</li>
                  <li>Pay per use. No additional fees</li>
                  <!-- <li>BONUS TRACK – Military Intelligence: Search through the Iraq and Afghan War Diaries</li> -->
              </ul>
              </div>
              <div id="benefits" class="card card-body mt-2 choosen" style="display: none">
                <ul>
                  <li>Reduce Information Gathering time processes and Invest on Análisis and Strategy</li>
                  <li>Smooth and secure transition from “Off” to “Online” researchs</li>
                  <li>Keep your Business Data Safe. if a Breach appears, "Effect Group" will find.</li>
                  <li>Reduce Business Risk impact. Our Search Engines allow Organizations to have the best Anti-Corruption knowledge, incluiding Money laundering practices.</li>
                  <li>Adaptative Intelligence. Real Time Web Data extraction. One “keyword”, multiple Sources, including Amazon S3 CDN Services</li>
                  <li>Fake News, Fake Profiles, Fake World… Discover the truth behind Images. AI Technologie to improve reality.</li>
                  <!-- <li>Keep your employees up to date and aware of Cyber Threats: Offsensive Tools to understand the dangers in an hiperconected world</li> -->
                  <li>Explore, extract and analyse the seen and hiden Social Networks Data. A world of relationships waiting to be discovered (Coming soon)</li>
                  <li>Access "Effect Group" anytime, anywhere, from any device. Our Saas Intelligence Systems are accessed via web browser. Responsive Design for a better User Experience</li>
                  <li>Take your research wherever you go. Intelligence Signal ChatBot available. All the Intel at your pocket.</li>
                  <li>High Privacy Standards. Private Servers in Sweden. End to end encription</li>
                  <li>Forget about multiple subscriptions and pay for what you really need in a Single Service. We optimize processes, but also resources.</li>
                </ul>
              </div>
            </div>
            
          </div>
      </div>

      <!-- <img src="img/wedo.png" class="header-five"> -->
    </div>

    <div id="useCase" class="bg-light sect" style="position: relative; padding-top: 50px; padding-bottom: 50px;">
      
      <div class="container text-center desktop">
        <h1 class="tx-spacing-2 mb-md-5 mb-3" style="font-size: 40px;"  data-aos="zoom-in">Use Cases <span class="tx-info">.</span></h1>
        <div class="row">
          
          <?php foreach ($useCase1 as $key): ?>
            <div class="col-md-3 col-6 align-self-end" data-aos="fade-down">
              <div class="card card-body mb-md-0 mb-3 shadow use-case" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modalOne">
                <h5 class="showin mb-0"><?= $key->title ?></h5>
                <h5 class="hiddin mb-0" style="display: none; color: #7757ff">Learn More</h5>
              </div>
            </div>
          <?php endforeach ?>
        
        </div>

        <img src="img/use-case.png" class="header-six" data-aos="zoom-in" data-aos-delay="1200">

        <div class="row">
          
          <?php foreach ($useCase2 as $key): ?>
            <div class="col-md-3 col-6 align-self-start" data-aos="fade-up">
              <div class="card card-body mb-md-0 mb-3 shadow use-case" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modalFive">
                <h5 class="showin mb-0"><?= $key->title ?></h5>
                <h5 class="hiddin mb-0" style="display: none; color: #7757ff">Learn More</h5>
              </div>
              
            </div>
          <?php endforeach ?>

        </div>
      </div>

      <div class="container handphone">
        <h1 class="tx-spacing-2 mb-md-5 mb-3" style="font-size: 40px;" data-aos="zoom-in">Use Cases <span class="tx-info">.</span></h1>
        <div class="row row-xs">

          <div class="col-12" data-aos="fade-down">
            <div class="card card-body mb-md-0 mb-3 shadow bordered">
              <h5 class="showin mb-0">Identity<br>Verification</h5>
              <hr>
              <p class="mb-0">Fast & Accuracy People Search Results, making life easir for Ciberintelligence
                Researches / Analysts, Marketing & Sales Departments, HR Agencies, Government
                Agencies, Insurance Companies, Journalist or Media Companies. Verify Identity, Locate
                Persons and Track their changes with the Powerfull “Effect Group” People Data Access</p>
            </div>
          </div>
            
          <div class="col-12" data-aos="fade-down" data-aos-delay="400">
            <div class="card card-body shadow mb-md-5 mb-3 bordered">
              <h5 class="showin mb-0" >Due<br>Diligence</h5>
              <hr>
              <p class="mb-0">Prevent Corporate Risks. Obtain the most reliable information from Partners to
                Affiliate Companies, all in One Click, in only One Space. Offshore Companies, Criminal
                and Money Laundering activities: From Individuals to Companies, “Effect Group” tracks
                the net and provides you the info.</p>
            </div>
          </div>
            
          <div class="col-12" data-aos="fade-down" data-aos-delay="700">
            <div class="card card-body shadow mb-md-5 mb-3 bordered">
              <h5 class="showin mb-0">Cyber Security &<br>CyberInt Agencies</h5>
              <hr>
              <p class="mb-0">
                Keep Organizations and Customers Data safe, with the most reliable Leaks Sources.
                Connect Personal, Professional, and Social information.
                Improve results with Image AI Prediction & Face Recognition.
                Explore in Deep through accurate People and Companies Data.
                "Effect Group" gives you all the information to turn it into Knowledge.</p>
            </div>
          </div>
          
          <div class="col-12" data-aos="fade-down" data-aos-delay="800">
            <div class="card card-body shadow mb-md-0 mb-3 bordered">
              <h5 class="showin mb-0">Enrichment<br>Data</h5>
              <hr>
              <p class="mb-0">Empower your Sales Team with accuracy Leads. Know and reach your Target with
                precision, filling up the gaps of inadequate data or inaccurate information to improve
                Sales, Marketing and ROI. Increase Customers Knowledge = Increase conversions.</p>
            </div>
          </div>

          <div class="col-12" data-aos="fade-down" data-aos-delay="1000">
            <div class="card card-body mb-md-0 mb-3 shadow bordered">
              <h5 class="showin mb-0">Fraud &<br>Insurance</h5>
              <hr>
              <p class="mb-0">Our Service can show People Real Identity, and the real Online and Offline
                connections between, which prevents fraud and scams. Separate Fake Data from Real
                Users. Locations, Assets… all the information needed to to identify debtors is provided
                by “Effect Group”. Even from people involved in Money Laundering.
                </p>
            </div>
          </div>

          <div class="col-12" data-aos="fade-down" data-aos-delay="1200">
            <div class="card card-body shadow mt-md-5 mt-0 mb-md-0 mb-3 bordered">
              <h5 class="showin mb-0">HR<br>Agencies</h5>
              <hr>
              <p class="mb-0">Find Top Talent or Managing your Business and Resourcing Challenges never have
                been so easy. Identify, verify and reach the right People matching your Organization
                Values. Reduces time and resources in selection processes: Effective candidate filtering.</p>
            </div>
          </div>

          <div class="col-12" data-aos="fade-down" data-aos-delay="1300">
            <div class="card card-body shadow mt-md-5 mt-0 mb-md-0 mb-3 bordered">
              <h5 class="showin mb-0">Journalists &<br>Media Companies</h5>
              <hr>
              <p class="mb-0">Research People & Companies in Only One Space, makes their work much faster and
                more effective, allowing generate accurate and quality content. In a Fake News world,
                where images are generated and published every second, source verification and
                content validation is a "Must Have", and we provide the Tools to help this work.
                </p>
            </div>
          </div>

          <div class="col-12"  data-aos="fade-down" data-aos-delay="1400">
            <div class="card card-body shadow mb-md-0 mb-3 bordered">
              <h5 class="showin mb-0">High Privacy &<br>Security Standards</h5>
              <hr>
              <p class="mb-0">Our Real Time Data-Driven runs through Sweden Private Servers, ready for a safe browse. We don’t share your data. We care about your privacy, protecting your right to be Anonymous</p>
            </div>
          </div>

        </div>
      </div>

    </div>

    <!-- <div id="egPhone" class="bg-primary-dark sect" style="padding-top: 80px; padding-bottom: 80px;position: relative">
      
      <div class="container">
          <div class="row">
            <div class="col-md-6">
              <img src="img/eg-phone.png" class="img-fluid mb-md-0 mb-5" data-aos="zoom-in">
            </div>
            <div class="col-md-6 mb-md-0 mb-4">
                <h1 class="tx-white tx-spacing-2 tx-semibold mb-3" style="font-size: 40px!important;">Introduce about EG Phone <span class="tx-info">.</span></h1>
                <h5 class="text-white mb-2"><i>Empowered by the combined expertise of the Internet privacy community.</i></h5>
                <p class="tx-spacing-1 text-white">
                  <ul class="text-white">
                    <li>When you use a phone with Effect Group OS you are empowered by the combined expertise of the Internet privacy community 'de-googled' Android.</li>
                    <li>Does not put your data in google's cloud or constantly report your location to google.</li>
                    <li>hone calls are opportunistically encrypted so nobody can listen in. You are warned when you make or receive an unencrypted phone call, text messages are encrypted and a timer can be set so they disappear.</li>
                    <li>Advertising and trackers blocked via DuckDuckGo Browser and DuckDuckGo as default search provider.</li>
                    <li>Built-in free "Virtual Private Network" services from trusted organizations protect you from being spied on. Private servers in Iceland.</li>
                    <li>Your phone is receiving regular, timely, automatic security updates.</li>
                    <li>Your data is backed up with strong encryption to your personal cloud server or to USB storage.Private cloud in Iceland.</li>
                    <li>Run on our own private servers in Iceland, data does not pass through any other systems. Possible to have a stealth SIM.</li>
                    <li>Enabling voice changing, phone number can be anything you wish, in any country. Locating of phone is impossible. Stealth SIM - includes unlimited roaming.</li>
                  </ul>
                  
            </div>
            
          </div>
          <div class="text-center mt-4">
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Cell site protection</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> No geolocation</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> IMEI Block</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Mesh channels</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Static or Dynamic caller ID</span> 
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Voice changer</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Worldwide roaming</span> 
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> No Billing record</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Random geolocation</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> VIP internal number</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Stealth Messeges</span>
            <span class="badge badge-primary mx-1 mb-2"><i data-feather="check" class="mr-1 wd-10"></i> Work in 160 country's</span>
          </div>
              
      </div>
    </div> -->

    <div id="requestQuote" class="bg-purple sect" style="padding-top: 80px; padding-bottom: 80px;">
      <div class="container text-center">
       
        <h1 class="callback">Need Help?</h1>
        <h5 style="color: #015de1">Write us or request a callback</h5>
        <a href="contact" class="btn btn-brand-01 bordered-01 shadow btn-lg px-4 mt-4 tx-uppercase tx-spacing-2">Yes, I Want Help <i data-feather="mail" class="ml-2"></i></a>
      </div>
      
    </div>

    

    <footer class="bg-dark py-4 px-3 text-white">
      <div class="row">
        <div class="col-md-6 text-md-left text-center mb-md-0 mb-3">
          <a href="javascript:void(0)" data-toggle="modal" data-target="#modalTerms" data-backdrop="static" data-keyboard="false" class="text-white mr-3">TERM OF USE</a> | <a href="legal" class="text-white ml-3">LEGAL</a>
        </div>
       
        <div class="col-md-6 text-md-right text-center">
          <a href="javascript:void(0)" class="text-white mr-3"><i class="fas fa-rss mr-2"></i> RSS Feeds</a> | <span class="ml-3">Copyright © 2021 - Effect Group</span>
        </div>
      </div>
    </footer>

    <div class="modal fade" id="modalTerms" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Term of Use</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              We treat your personal data as confidential and in accordance with the statutory data protection regulations and this privacy policy. If you use this website, various pieces of personal data will be collected. Personal information is any data with which you could be personally identified.

              The content of these pages was created with great care. Nevertheless, Effect Group can assume no responsibility for the accuracy, completeness or currency of their content.
              
              Exclusion of Liability (Disclaimer), November 2020.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalOne" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Identity Verification</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Fast & Accuracy People Search Results, making life easir for Ciberintelligence
              Researches / Analysts, Marketing & Sales Departments, HR Agencies, Government
              Agencies, Insurance Companies, Journalist or Media Companies. Verify Identity, Locate
              Persons and Track their changes with the Powerfull “Effect Group” People Data Access
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalTwo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Due Diligence</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Prevent Corporate Risks. Obtain the most reliable information from Partners to
              Affiliate Companies, all in One Click, in only One Space. Offshore Companies, Criminal
              and Money Laundering activities: From Individuals to Companies, “Effect Group” tracks
              the net and provides you the info.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalThree" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Cyber Security And Cyber Int Agencies</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Keep Organizations and Customers Data safe, with the most reliable Leaks Sources.
              Connect Personal, Professional, and Social information.
              Improve results with Image AI Prediction & Face Recognition.
              Explore in Deep through accurate People and Companies Data.
              "Effect Group" gives you all the information to turn it into Knowledge
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalFour" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Enrichment Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Empower your Sales Team with accuracy Leads. Know and reach your Target with
              precision, filling up the gaps of inadequate data or inaccurate information to improve
              Sales, Marketing and ROI. Increase Customers Knowledge = Increase conversions.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalFive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Fraud & Insurance</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Our Service can show People Real Identity, and the real Online and Offline
              connections between, which prevents fraud and scams. Separate Fake Data from Real
              Users. Locations, Assets… all the information needed to to identify debtors is provided
              by “Effect Group”. Even from people involved in Money Laundering.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalSix" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">HR Agencies</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Find Top Talent or Managing your Business and Resourcing Challenges never have
              been so easy. Identify, verify and reach the right People matching your Organization
              Values. Reduces time and resources in selection processes: Effective candidate filtering
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalSeven" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Journalists And Media Companies</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
              Research People & Companies in Only One Space, makes their work much faster and
              more effective, allowing generate accurate and quality content. In a Fake News world,
              where images are generated and published every second, source verification and
              content validation is a "Must Have", and we provide the Tools to help this work.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalEight" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">High Privacy And Security Standards</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="mb-0">
            Our Real Time Data-Driven runs through Sweden Private Servers, ready for a safe browse. We don’t share your data. We care about your privacy, protecting your right to be Anonymous
            </p>
          </div>
        </div>
      </div>
    </div>

    <div id="vidBox">
      <div id="videCont">
        <video id="videoEg" loop controls>
          <!-- <source src="1.webm" type="video/webm"> -->
          <source src="popup/effect-group.mp4" type="video/mp4">
         </video>
      </div>
    </div>

    <!-- Bootstrap-Cookie-Alert -->
    <div id="cookies" class="cookiealert p-4" role="alert" style="display: none">
      <div class="card card-body shadow text-center">
            <div class="text-center my-4">
              <img src="cookie.png" style="width: 50px">
            </div>
           
            <h2>We use cookies</h2>
            <p>This website use cookies to give you the best, most relevant experince. By continuing to use this website you'are accepting this</p>
            <div class="text-center">
            <button class="btn btn-primary mt-4" onclick="accpetCookie()" style="width: 100px">
              Confirm
            </button>
            </div>
       
      </div>
    </div>
    <!-- END Bootstrap-Cookie-Alert -->

    <!-- <div class="modal fade" id="samHaveABoy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              <img src="img/dott.jpg" class="img-fluid">
          </div>
        </div>
      </div>
    </div> -->

    <script src="//code.tidio.co/o0cylckokzypqxb4t2kajinc27qug63v.js" async></script>
      

    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/feather-icons/feather.min.js"></script>
    <script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="popup/videopopup.js"></script>
    <script src="assets/js/jquery.background-video.js"></script>
    <script src="assets/js/dashforge.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init({
          duration: 1200,
        })
    </script>
    <script>
        if (getCookie('accepted') === 'yes') {
            document.getElementById("cookies").style.display = "none";
        } else {
          setTimeout(function(){ document.getElementById("cookies").style.display = "block"; }, 5000);
        }

        // user clicks the confirmation -> set the 'yes' value to cookie and set 'accepted' as name
        function accpetCookie() {
            setCookie('accepted', 'yes', 100);
            document.getElementById("cookies").style.display = "none";
        }

        // code from :http://stackoverflow.com/a/4825695/191220
        // set cookie method
        function setCookie(c_name, value, exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = c_name + "=" + c_value;
        }

        // get cookie method   
        function getCookie(c_name) {
            var i, x, y, ARRcookies = document.cookie.split(";");
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                x = x.replace(/^\s+|\s+$/g, "");
                if (x == c_name) {
                    return unescape(y);
                }
            }
        }
    </script>
    <script>
        // if user has already checked the confirmation button
        // the alert panel should be hidden.   
        
      $(document).ready(function(){
        $(".preloader").delay(1200).fadeOut(); 

       

        var elem = window.location.hash.replace('#', '');
        if(elem) {
          $('html, body').animate({
              scrollTop: $("#S" + elem).offset().top - 50
          }, 1000);
        }
        
        
        $('.decks').each(function(){  
          
          // Cache the highest
          var highestBox = 0;
          
          // Select and loop the elements you want to equalise
          $('.deck', this).each(function(){
            
            // If this box is higher than the cached highest then store it
            if($(this).height() > highestBox) {
              highestBox = $(this).height(); 
            }
          
          });  
                
          // Set the height of all those children to whichever was highest 
          $('.deck',this).height(highestBox);
                        
        }); 

      });

      $(function () {
        $('#vidBox').VideoPopUp({
          opener: "video-trigger",
          idvideo: "videoEg",
          backgroundColor:"#000000",
          pausevideo:true

          });
        });
      $(".links").click(function() {
        var target = $(this).attr("data-target");
        $('body').removeClass('navbar-nav-show');
          $('html, body').animate({
              scrollTop: $("#" + target).offset().top - 50
          }, 1000);
      });

      $(".choose").click(function(){
        var target = $(this).attr("data-target");


        $(".choose").removeClass("btn-brand-01 btn-link").addClass("btn-link");
        $(this).removeClass("btn-brand-01 btn-link").addClass("btn-brand-01");
        $(".choosen").hide();
        $("#" + target).fadeIn();
      });

      $(".screenshot").click(function(){
        var target = $(this).attr("data-target");


        $(".screenshot").removeClass("hovereds");
        $(this).addClass("hovereds");
        $(".fitur").hide();
        $("#" + target).fadeIn();
      });

      $('.use-case').hover(
        function(){
          var $this = $(this);
          $this.addClass("bordered");
          $this.find(".showin").hide();
          $this.find(".hiddin").show();
        },
        function(){
          var $this = $(this);
          $this.removeClass("bordered");
          $this.find(".hiddin").hide();
          $this.find(".showin").show();
        }
      );
      // $(window).scroll(function() {
      //   var x = $(".navbar").offset().top - 100;
      //   $(".sect").each(function(index) {
      //     var z = $(this).attr("id");
      //     if (x > $(this).offset().top && x <= $(this).offset().top + $(this).height()) {
      //       $('#S' + z).addClass("active");
      //     } else {
      //       $('#S.' + z).removeClass("active");
      //     }
      //   })
      // });


    </script>
    <!--- Bazo code --->
    <!-- Fathom - beautiful, simple website analytics -->
<script src="https://cdn.usefathom.com/script.js" data-site="UAPNJREX" defer></script>
<!-- / Fathom -->
  </body>
</html>
