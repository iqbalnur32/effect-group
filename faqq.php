<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="ALL IN ONE INTEL DATA">
    <meta name="author" content="Effect Group">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/eg-icon.png">

    <title>Frequently Asked Questions - Effect Group | Business Intelligence Operations Base</title>

    <!-- vendor css -->
    <link href="lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/dashforge.landing.css">
    <!-- <link rel="stylesheet" href="assets/css/jquery.background-video.css"> -->
    <link rel="stylesheet" href="popup/videopopup.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css">
    <style>
      html {
        overflow-x: hidden;
      }

      #home {
        height: 100vh;
        margin-top: 0px;
        background-color: black;
      }

      .shadowed {
        -webkit-box-shadow: 10px 10px 14px -7px rgba(0,0,0,0.33);
        -moz-box-shadow: 10px 10px 14px -7px rgba(0,0,0,0.33);
        box-shadow: 10px 10px 14px -7px rgba(0,0,0,0.33);
      }

      #myVideo {
        /* padding-top: 0px; */
        position: absolute;
        top: 0;
        /* margin-top: -40%; */
        right: 0;
        /* bottom: 0; */
        min-width: 100%;
        min-height: 100%;
        height: 100vh;
        object-fit: fill;
        opacity: 0.5;
      }
      .screenshot {
        cursor: pointer;
      }

      .hovereds {
        border-left: 5px solid #001737;
      }
      .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: #141C2B;
      }

      .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
      }

      .fitur {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
      }

      .callback {
        font-size: 50px;
        color: #015de1;
      }
      .bg-dark {
        background-color: black!important;
      }
      .bg-purple {

        background-image: url("img/network-left.png"),  url("img/network.png"); /* The image used */
        background-color:white; /* Used if the image is unavailable */
        background-position: left, right; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: 40%, 40%; /* Resize the background image to cover the entire container */
      }
      .badge-primary {
        background-color: #7757ff;
      }
      .check {
        background-color: #7757ff;
        padding: 5px 10px;
        border-radius: 3px;
        color: white;
      }
      .use-case {
        cursor: pointer;
      }

      .nav-item.active {
        color: #7757ff!important;
        letter-spacing: 3px;
        text-transform: uppercase;
      }
      .bordered {
        border: 3px solid #7757ff;
      }

      .bordered-01 {
        border: 2px solid #001737;
      }
      .absoluted {
        position: absolute;
        width: 100%;
      }
      .header-one {
        width: 40%;
        position: absolute;
        top: 23%;
        right: 5%;
        z-index: 3;
        /* transform: rotate(30deg); */
      }
      .header-two {
        width: 30%;
        position: absolute;
        top: 8%;
        right: 25%;
        z-index: 1;
        /* transform: rotate(30deg); */
      }
      .header-three {
        width: 50%;
        position: absolute;
        top: 2%;
        right: 0%;
        z-index: 2;
        opacity: 0.1;
        /* transform: rotate(30deg); */
      }

      .headers-one {
        width: 50%;
        position: absolute;
        top: 2%;
        left: 0%;
        z-index: 2;
        opacity: 0.1;
        /* transform: rotate(30deg); */
      }

      .header-ss-1 {
        /* width: 50%; */
        position: absolute;
        top: -10%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      .header-four {
        opacity: 0.2;
        width: 45%;
        position: absolute;
        top: 8%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }
      .header-five {
        opacity: 0.1;
        width: 65%;
        position: absolute;
        top: -30%;
        right: -13%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      .header-six {
       width: 80%;
       margin-top: -50px;
       margin-bottom: -50px;
      }

      .text-nav {
        color: white!important;
      }

      .desktop {
        display: block;
      }

      .handphone {
        display: none;
      }

      @media (min-width: 320px) and (max-width: 480px) {
      .header-five {
        display: none;
      }
      
      .fitur {
        position: relative;
      }


      #home {
        height: 100%;
        margin-top: 20px;
        background-color: black;
        padding-top: 50px;
        padding-bottom: 50px;
      }

      #myVideo {
        /* padding-top: 0px; */
        position: absolute;
        top: 0;
        /* margin-top: -40%; */
        right: 0;
        /* bottom: 0; */
        min-width: 100%;
        min-height: 100%;
        /* height: 100vh; */
        /* object-fit: fill; */
        opacity: 0.5;
      }

      .desktop {
        display: none;
      }

      .handphone {
        display: block;
      }

      .text-nav {
        color: #001737!important;
      }
      .callback {
        font-size:35px;
      }
      .bg-purple {
        background-image: url(""); /* The image used */
        background-color:white; /* Used if the image is unavailable */
      }
      .header-four {
        display: none!important;
      }

      .header-six {
        display: none!important;
      }

      .use-case {
        height: 100px;
        border: 3px solid #7757ff;
      }
  
        .header-one {
        width: 75%;
        position: absolute;
        top: 15%;
        left: 20%;
        z-index: 3;
        /* transform: rotate(30deg); */
      }
      .header-two {
        width: 50%;
        position: absolute;
        top: 10%;
        left: 8%;
        z-index: 1;
        /* transform: rotate(30deg); */
      }
      .header-three {
        width: 80%;
        position: absolute;
        top: 8%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      
        
      }
    </style>

  </head>
  <body class="home-body pt-3">
    <div class="preloader">
      <div class="loading">
        <img src="https://dashboard.effect.group/img/loaders.gif" width="300">
      </div>
    </div>

    <?php include "layout/header_navigation.php" ?>      

    <div id="requestQuote" class="sect" style="padding-top: 80px; padding-bottom: 80px;">
      <div class="bg-purple py-5 text-center">
          <h1 class="callback tx-semibold">Frequently Asked Question</h1>
        </div>
      <div class="container text-center">
        
        <!-- <h5 class="mb-5" style="color: #015de1">Write us or request a callback</h5> -->

        <div class="row justify-content-center">
          <div class="col-md-10">


              <h3 class="my-5 text-center">SNAPS</h3>
              <div class="accordion shadowed bg-white text-left">
                
                <h6>What is a “Snap”?</h6>
                <div>“Snaps” are the currency of the Effect Group. A kind of credits that are included in the "Base
                Subscription" and that can also be purchased in different amounts Packs. “Snaps” are
                necessary for running our Services, which can have differents "Snaps" values depending on
                their type. As an example, one “People Search” have a different “Snap” value than a “Business
                Search”.<br>
                <div class="text-center my-5">
                  <img src="img/Snap.jpg" class="img-fluid" style="border-radius: 5px">
                </div>
                Except for those Services included in the "Effect Club" Loyalty Programme (See point 3 of this
                F.A.Q, or Click here for more info), "Snaps" will be required to use Effect Group Services. 
                </div>

                <h6>How can I buy Snaps?</h6>
                <div>“Snaps” can be purchased in two different ways: Through the monthly “Base Subscription”
                and, additionally, by purchasing one of the three available "Snap Packs". The “Base
                Subscription” includes a fixed amount of 10 Snaps, while Packs vary between 5 and 40 Snaps.
                See Effect Group Price List for more info. </div>

                <h6>Which Payment Methods can I use?</h6>
                <div>Effect Group currently offers two Payment Gateways: Paypal and Stripe. We want to maintain
                our commitment to trust, security and privacy, so all your data and transaction history will be
                protected with the highest security standards. 
                <div class="text-center my-5">
                  <img src="img/Paypal_Logo.png" class="mr-3" style="width: auto; height: 50px"> <img src="img/Stripe_Logo_2.png" style="width: auto; height: 50px">
                </div>
                </div>

                <h6>Do I need a subscription to buy "Snap's Packs"? </h6>
                <div>Yes. You need to have an active susbscription to can buy “Snap Packs”. After all, the “Base
                Subscription” is similar to a "Snap Pack". "Snap Packs" are designed to cover those Clients who,
                due to their type of work, need a higher volume of Searches than those “Snaps” included in
                the "Base Subscription" </div>

                <h6>“Snaps” can Expire?</h6>
                <div>Yes. Each “Snap” has a time to be used. This time will be longer depending the number of
                “Snaps” that have been purchased, and varies from a minimum of one month for smaller Packs
                to a maximum of three months for Larger Packs, as it is detailed in our current Price List. The
                “Snaps” included in our “Base Subscription”, has one month time expire. 
                <br><br>
                We will inform you at all times of your available "Snap" balance, as well as the time remaining
                for the use of your "Snaps" 

                <div class="text-center my-5">
                  <img src="img/Snap_Widget.jpg" class="img-fluid mb-3" style="border-radius: 5px"><br>
                  <img src="img/Snap_Expiration.jpg" class="img-fluid" style="border-radius: 5px">
                </div>

                </div>
              </div>
              <h3 class="my-5 text-center">SEARCHES</h3>
              <div class="accordion shadowed bg-white text-left">

                <h6>What is covered by a “People Search”?</h6>
                <div>When you introduce an Input (Full Name, Email or Phone Number) in our Dashboard, and you
                Click on the “Search Button”, this action is considered a "People Search", and you will be
                charged with one "Snap"
                  <div class="text-center my-5">
                    <img src="img/people_Search.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>

                  Each “People Search” covers the following Results:<br>
                  <ul>
                    <li>“Pre-Snap”: A list of multiple Targets that match with the query</li>
                    <li>“Person Data”: Name, Gender, Lenguages , DOB, Work Experience… related to the Target</li>
                    <li>“Leaked Data”: Passwords, Usernames, IP’s, Phone Numbres…. related to an Email</li>
                    <li>“Social Accounts”: Accounts in mayor Social Networks and other Web / Mobile Services related
                    to an Email</li>
                    <li>“Phone Data”: Accounts in mayor Social Networks and other Mobile Services related to a
                    Phone Number </li>
                    <li>“UK Residents” (Only if “UK Residentes Box” is Checked): Physical Address related to the
                    Target, and acces to “UK Searches”. Additional “Snaps” will be charged, according to “Effect Group” Price List</li>
                    <li>“Officers”: Name, Company, Occupation, Position, Nationality, DOB… Available only If the target has a
                    Directive Position in a Company</li>

                   

                  </ul>

                  <div class="text-center my-5">
                    <img src="img/People_Search_Data_Points.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>

                </div>

                <h6>What kind of Inputs can be introduced in "People Search"?</h6>
                <div>In "Effect Group", you can start your Research through three different Inputs: Full Name, Email
                or Mobile Phone Number. Regardless of the type of Input entered, our AI Based Systems will
                trace the entire network in search of related results. In most cases, entering a single Input is
                enough to obtain the desired results. 
                  <div class="text-center my-5">
                    <img src="img/People_Search_Inputs.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                  However, the more Inputs related to the Target to be investigated are entered, the more
                  accurate and higher quality results will be obtained. 

                </div>

                

                <h6>Does a "People Search" have the same price depending on the Input entered? </h6>
                <div>Yes. “People Search” have a unique Price, regardless of whether the Input is a Full Name, an
                Email or a Mobile Phone Number, cause Reults obtained are usually similar and of high quality
                if we can establish the right relationships between the different Data Points.<br>
                Actually, an according our current Price List, that Price is established in one “Snap”  
                </div>

                <h6>What is a "Pre-Snap"? </h6>
                <div>A “Pre-Snap” is a preview of the results related to the Input entered in "People Search". This
                tool is very useful, as it allows us to choose, without mistakes, the Target we are really
                interested in, avoiding wasting time and money in our Researchs 
                  <div class="text-center my-5">
                    <img src="img/Pre-Snap.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                  Once you have selected one of the Targets appearing in the "Pre-Snap", all related Data Points
                  will be displayed. 

                </div>

                <h6>Can I choose more than one Target in a “Pre-Snap”?</h6>
                <div>Yes. You can choose any of the multiple options that appear in the "Pre-Snaps", which is the
                list of results matching the Input entered in "People Search", as said above. But please note
                that a “People Search” only covers the research of one of the results listed in the “Pre-Snap”
                List, so if you select more than one, 0,50 "Snaps" will be charged to your Account, according to
                our current Price List.</div>

                <h6>What is a “Re-Search”?</h6>
                <div>It's easy to explain. When you do a "People Search", it is common that our systems returns a
                lots of data related to the Target. Effect Group, by default, and in case they exists, analyses
                one Email and one Phone Number (giving preference to personal ones) from among those that
                our Systems have found. 

                  <div class="text-center my-5">
                    <img src="img/Different_Phones_and_Emails_in_Re-Searchs.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>

                  But if you want to research any other Email or Phone Number that appears in the Results
                  related to the Target, you can do so. We call this new search "Re-Search". The option to access
                  it will be displayed on your screen through a POPUP once our Systems have analysed all the
                  previous data from the selected Target. <br><br>
                  Since this “Re-Search” does not require the need to re-scan the full network, it cost will be less
                  than a normal "People Search": 0,30 "Snaps" for Emails, and 0,10 "Snaps" for Phone Numbers,
                  according to our current Price List. 

              
                </div>

                <h6>Can I search for an Email or Phone Number different than the ones that appear in the Results
                List?</h6>
                <div>Yes, of course. If you know an Email or Phone Number related to the Target, but it doesn’t
                appear in the list of results that Effect Group provides you with, you can always go back to the
                main "People Search" Menu and use it as Input for a new search. In this case, this new search
                will be considered as a "People Search" and not as a "Re-Search", and therefore, the current
                prices in our Price List will be applied.</div>

                <h6>What are “Re-Fetchs” in Phone data or Social Accounts Results?</h6>
                <div>Occasionally, it is possible that in the Results we extract from both Emails and Mobile Phone
                Numbers, some Services wont be loaded at the same time. This is because crawling such a
                large number of Services takes time, so if the Search for an Account in a particular Service is
                delayed, we cut it off, to give you the rest of the Search Results faster. 
                <div class="text-center my-5">
                    <img src="img/Re-Fetch_button_in_Facebook_Service.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                But if you click the "Re-Fetch" button, our spider will re-fetch that account on that Service, at
                no additional cost, until it is found. 

                </div>

                <h6>Will I be charged if a click in the “Re-Fetch” Button?</h6>
                <div>As said above, never! Click on the "Re-Fecth" button as many times as you want until we can
                provide you with the results for that Service. We will never charge you!
                

                </div>

                <h6>Will I be charged if a “People Search” doesn’t return any Result? </h6>
                <div>No. If our AI Based Sysytem does’t find any result, they are probably not available in the Net.
                But you won’t be charged. Effect Group only charges if it is able to give our Clients the
                information they need, if it is available. </div>

                <h6>All data points will be displayed in all Search Results?</h6>
                <div>It will depend on the exposure and positions in Companies of the Target being analysed. The
                greater online exposure of the Target, the more Data Points we will be able to find and,
                therefore, to show. Keep in mind that, as an example, not all Targets are UK Residents, or have
                a Company Position, or simple an Email related haven’t been found. In that cases, not all Data
                Points can be shown. But as we use to say, if it’s not in Effect Group, is not on the Net. </div>

                <h6>“UK Residents” option is a paid Service?</h6>
                <div>Yes. We need to search through Public and paid Registers so, the service has a small additional
                cost. Take a look to our current Price List. </div>

                <h6>Do I have to do anything to get results for "UK Residents" in my search?</h6>
                <div>Yes. But don't worry! All you have to do is "Check the Box"
                  <div class="text-center my-5">
                    <img src="img/Check_the_Box_UK_Residents.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                </div>

                <h6>Is possible to access my Account Search History?</h6>
                <div>Yes. All the searches you have made can be consulted as many times as you want through your
                  " History" widget, available in your Personal Dashboard. 
                  <div class="text-center my-5">
                    <img src="img/Effect_Group_History.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                </div>

                <h6>If I click in an stored Search, Will I be charged?</h6>
                <div>Obviously not. We have already done our work to give you the best results. So we won't
                charge you again.</div>

                <h6>If I change my User Account, will I be able to transfer my search history to my new Account?  </h6>
                <div>Unfortunately not. For Privacy and Security reasons, “History” Data is associated to a unique
                ID, so it can’t be transferred into a different one. </div>

                <h6>Can I export Search Results?</h6>
                <div>Yes. You can export your Search Results clicking the “Download PDF” Button in the top right of
                the Screen Results
                <div class="text-center my-5">
                    <img src="img/Export_PDF_Button.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                </div>

                <h6>Is it possible to access Effect Group Services via API? </h6>
                <div>Yes. Our API is currently under development, but the following endpoints (POST) are already
                available: 
                  <div class="text-center my-5">
                    <img src="img/api.png" class="img-fluid" style="border-radius: 5px">
                  </div>
                  Also you will be able to check your “History” and the “Snaps Expiration Time” (GET). New
                  features, as “UK Residents”, will be available soon. 
                  <div class="text-center my-5">
                    <img src="img/Apimatic_API_Doc_Screenshot.jpg" class="img-fluid" style="border-radius: 5px">
                  </div>
                  For more detailed info about our API Services, please visit our <a href="https://www.apimatic.io/apidocs/effect-group/v/1_0" target="_blank">API DOC</a> and our current Price
                  List.

                </div>
              </div> 
          </div>
        </div>
      </div>
      
    </div>

    

    

    <footer class="bg-dark py-4 px-3 text-white">
      <div class="row">
        <div class="col-md-6 text-md-left text-center mb-md-0 mb-3">
          <a href="javascript:void(0)" data-toggle="modal" data-target="#modalTerms" data-backdrop="static" data-keyboard="false" class="text-white mr-3">TERM OF USE</a> | <a href="legal" class="text-white ml-3">LEGAL</a>
        </div>
       
        <div class="col-md-6 text-md-right text-center">
          <a href="javascript:void(0)" class="text-white mr-3"><i class="fas fa-rss mr-2"></i> RSS Feeds</a> | <span class="ml-3">Copyright © 2021 - Effect Group</span>
        </div>
      </div>
    </footer>

    <!-- Bootstrap-Cookie-Alert -->
    <div class="alert text-center cookiealert" role="alert">
        <b>Do you like cookies?</b> &#x1F36A; We use cookies to ensure you get the best experience on our website. <a href="https://cookiesandyou.com/" target="_blank">Learn more</a>
    
        <button type="button" class="btn btn-primary btn-sm acceptcookies">
            I agree
        </button>
    </div>
    <!-- END Bootstrap-Cookie-Alert -->
      
    

    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/feather-icons/feather.min.js"></script>
    <script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="popup/videopopup.js"></script>
    <script src="assets/js/jquery.background-video.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>

    <script src="assets/js/dashforge.js"></script>
    <script>
      window.addEventListener("cookieAlertAccept", function() {
          alert("cookies accepted");
      })
      $(document).ready(function(){
        $(".preloader").delay(1200).fadeOut(); 


      });

      $(".links").click(function() {
        var target = $(this).attr("data-target");
        $('body').removeClass('navbar-nav-show');
          $('html, body').animate({
              scrollTop: $("#" + target).offset().top - 50
          }, 1000);
      });

      $(".choose").click(function(){
        var target = $(this).attr("data-target");


        $(".choose").removeClass("btn-brand-01 btn-link").addClass("btn-link");
        $(this).removeClass("btn-brand-01 btn-link").addClass("btn-brand-01");
        $(".choosen").hide();
        $("#" + target).fadeIn();
      });

      $(".screenshot").click(function(){
        var target = $(this).attr("data-target");


        $(".screenshot").removeClass("hovereds");
        $(this).addClass("hovereds");
        $(".fitur").hide();
        $("#" + target).fadeIn();
      });

      $('.use-case').hover(
        function(){
          var $this = $(this);
          $this.addClass("bordered");
          $this.find(".showin").hide();
          $this.find(".hiddin").show();
        },
        function(){
          var $this = $(this);
          $this.removeClass("bordered");
          $this.find(".hiddin").hide();
          $this.find(".showin").show();
        }
      );
      // $(window).scroll(function() {
      //   var x = $(".navbar").offset().top - 100;
      //   $(".sect").each(function(index) {
      //     var z = $(this).attr("id");
      //     if (x > $(this).offset().top && x <= $(this).offset().top + $(this).height()) {
      //       $('#S' + z).addClass("active");
      //     } else {
      //       $('#S.' + z).removeClass("active");
      //     }
      //   })
      // });

      $('.accordion').accordion({
          heightStyle: 'content',
          collapsible: true
      });


    </script>
  </body>
</html>
