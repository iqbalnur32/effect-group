<?php 
  include 'sys/db.php';
  include 'sys/config.php';  
  error_reporting(0);
  $c = new Config();
  $db = new Database();
  $Contentfaq = $db->query("SELECT * FROM content_faq")->rowObject(); // Faq Content
?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="ALL IN ONE INTEL DATA">
    <meta name="author" content="Effect Group">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/eg-icon.png">

    <title>Frequently Asked Questions - Effect Group | Business Intelligence Operations Base</title>

    <!-- vendor css -->
    <link href="lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/dashforge.landing.css">
    <!-- <link rel="stylesheet" href="assets/css/jquery.background-video.css"> -->
    <link rel="stylesheet" href="popup/videopopup.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css">
    <style>
      html {
        overflow-x: hidden;
      }

      #home {
        height: 100vh;
        margin-top: 0px;
        background-color: black;
      }

      .shadowed {
        -webkit-box-shadow: 10px 10px 14px -7px rgba(0,0,0,0.33);
        -moz-box-shadow: 10px 10px 14px -7px rgba(0,0,0,0.33);
        box-shadow: 10px 10px 14px -7px rgba(0,0,0,0.33);
      }

      #myVideo {
        /* padding-top: 0px; */
        position: absolute;
        top: 0;
        /* margin-top: -40%; */
        right: 0;
        /* bottom: 0; */
        min-width: 100%;
        min-height: 100%;
        height: 100vh;
        object-fit: fill;
        opacity: 0.5;
      }
      .screenshot {
        cursor: pointer;
      }

      .hovereds {
        border-left: 5px solid #001737;
      }
      .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: #141C2B;
      }

      .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
      }

      .fitur {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
      }

      .callback {
        font-size: 50px;
        color: #015de1;
      }
      .bg-dark {
        background-color: black!important;
      }
      .bg-purple {

        background-image: url("img/network-left.png"),  url("img/network.png"); /* The image used */
        background-color:white; /* Used if the image is unavailable */
        background-position: left, right; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: 40%, 40%; /* Resize the background image to cover the entire container */
      }
      .badge-primary {
        background-color: #7757ff;
      }
      .check {
        background-color: #7757ff;
        padding: 5px 10px;
        border-radius: 3px;
        color: white;
      }
      .use-case {
        cursor: pointer;
      }

      .nav-item.active {
        color: #7757ff!important;
        letter-spacing: 3px;
        text-transform: uppercase;
      }
      .bordered {
        border: 3px solid #7757ff;
      }

      .bordered-01 {
        border: 2px solid #001737;
      }
      .absoluted {
        position: absolute;
        width: 100%;
      }
      .header-one {
        width: 40%;
        position: absolute;
        top: 23%;
        right: 5%;
        z-index: 3;
        /* transform: rotate(30deg); */
      }
      .header-two {
        width: 30%;
        position: absolute;
        top: 8%;
        right: 25%;
        z-index: 1;
        /* transform: rotate(30deg); */
      }
      .header-three {
        width: 50%;
        position: absolute;
        top: 2%;
        right: 0%;
        z-index: 2;
        opacity: 0.1;
        /* transform: rotate(30deg); */
      }

      .headers-one {
        width: 50%;
        position: absolute;
        top: 2%;
        left: 0%;
        z-index: 2;
        opacity: 0.1;
        /* transform: rotate(30deg); */
      }

      .header-ss-1 {
        /* width: 50%; */
        position: absolute;
        top: -10%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      .header-four {
        opacity: 0.2;
        width: 45%;
        position: absolute;
        top: 8%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }
      .header-five {
        opacity: 0.1;
        width: 65%;
        position: absolute;
        top: -30%;
        right: -13%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      .header-six {
       width: 80%;
       margin-top: -50px;
       margin-bottom: -50px;
      }

      .text-nav {
        color: white!important;
      }

      .desktop {
        display: block;
      }

      .handphone {
        display: none;
      }

      .cookiealert {
        position: fixed;
        bottom: 0; 
        left: 0; 
        width: 50%; 
        z-index: 9998
      }

      @media (min-width: 320px) and (max-width: 480px) {
      .header-five {
        display: none;
      }

      .cookiealert {
        position: fixed;
        bottom: 0; 
        left: 0; 
        width: 100%; 
        z-index: 9998
      }
      
      .fitur {
        position: relative;
      }


      #home {
        height: 100%;
        margin-top: 20px;
        background-color: black;
        padding-top: 50px;
        padding-bottom: 50px;
      }

      #myVideo {
        /* padding-top: 0px; */
        position: absolute;
        top: 0;
        /* margin-top: -40%; */
        right: 0;
        /* bottom: 0; */
        min-width: 100%;
        min-height: 100%;
        /* height: 100vh; */
        /* object-fit: fill; */
        opacity: 0.5;
      }

      .desktop {
        display: none;
      }

      .handphone {
        display: block;
      }

      .text-nav {
        color: #001737!important;
      }
      .callback {
        font-size:35px;
      }
      .bg-purple {
        background-image: url(""); /* The image used */
        background-color:white; /* Used if the image is unavailable */
      }
      .header-four {
        display: none!important;
      }

      .header-six {
        display: none!important;
      }

      .use-case {
        height: 100px;
        border: 3px solid #7757ff;
      }
  
        .header-one {
        width: 75%;
        position: absolute;
        top: 15%;
        left: 20%;
        z-index: 3;
        /* transform: rotate(30deg); */
      }
      .header-two {
        width: 50%;
        position: absolute;
        top: 10%;
        left: 8%;
        z-index: 1;
        /* transform: rotate(30deg); */
      }
      .header-three {
        width: 80%;
        position: absolute;
        top: 8%;
        right: 0%;
        z-index: 0;
        /* transform: rotate(30deg); */
      }

      
        
      }
    </style>

  </head>
  <body class="home-body pt-3">
    <div class="preloader">
      <div class="loading">
        <img src="https://dashboard.effect.group/img/loaders.gif" width="300">
      </div>
    </div>

    <header class="navbar navbar-header navbar-header-fixed bd-b-0 bg-primary">
      <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <div class="navbar-brand">
        <a href="./" class="df-logo text-white pl-md-5">
          <img src="img/headers.png" style="width: auto; height: 25px;">
        </a>
        
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="./" class="df-logo">Effect Group</a>
          <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu">          
          <li id="Shome" class="nav-item"><a href="./" class="nav-link text-nav">Home</a></li>
          <li id="SaboutUs" class="nav-item"><a href="./#aboutUs" class="nav-link text-nav">What We Do</a></li>
          <li id="SuseCase" class="nav-item"><a href="./#useCase" class="nav-link text-nav">Use cases</a></li>
          <!-- <li id="SegPhone" class="nav-item"><a href="./#egPhone" class="nav-link text-nav">EG phone</a></li> -->
          <li class="nav-item"><a href="javascript:void(0)" class="nav-link text-nav"><u>FAQ</u></a></li>
          <li class="nav-item"><a href="learning-center" class="nav-link text-nav">Learning Center</a></li>
      
          <li class="nav-item"><a href="effect-club" class="nav-link text-nav">Effect Club <span class="badge badge-danger ml-1" style="font-size: 6px; margin-top: -12px">NEW</span></a></li>
          <li class="nav-item"><a href="pricing" class="nav-link text-nav">Pricing</a></li>
          <!-- <li id="SrequestQuote" class="nav-item links" data-target="requestQuote"><a href="javascript:void(0)" class="nav-link text-nav">Request a Quote</a></li> -->
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right">
        <a href="https://dashboard.effect.group/registration" target="_blank" class="btn btn-brand-01 btn-sm tx-md-14 px-2 py-1 tx-8 mr-2">SIGN UP</a>
        <a href="https://dashboard.effect.group/login" target="_blank" class="btn btn-light btn-sm tx-md-14 tx-8 px-2 py-1 text-primary">LOGIN</a>
      </div>
    </header><!-- navbar -->

    

    <div id="requestQuote" class="sect" style="padding-top: 80px; padding-bottom: 80px;">
      <div class="bg-purple py-5 text-center">
          <h1 class="callback tx-semibold">Frequently Asked Question</h1>
        </div>
      <div class="container text-center">
        
        <!-- <h5 class="mb-5" style="color: #015de1">Write us or request a callback</h5> -->

        <div class="row justify-content-center">
          <div class="col-md-10">
              <?php foreach ($Contentfaq as $key): $category = $db->getEachTable('category_faq', 'id_category', $key->id_category)->rowObject() ?>

                  <h3 class="my-5 text-center"><?= $category[0]->name_category ?></h3>
                  <div class="accordion bg-white text-left">
                    <?php foreach (json_decode($key->title) as $title): ?>
                      <h6><?= $title ?></h6>
                    <?php endforeach ?>
                    <?php foreach (json_decode($key->description) as $description): ?>
                      <div><?= $description ?></div>
                    <?php endforeach ?>
                    <!-- <div>“Snaps” are the currency of the Effect Group. A kind of credits that are included in the "Base
                    Subscription" and that can also be purchased in different amounts Packs. “Snaps” are
                    necessary for running our Services, which can have differents "Snaps" values depending on
                    their type. As an example, one “People Search” have a different “Snap” value than a “Business
                    Search”.<br>
                    <div class="text-center my-5">
                      <img src="img/Snap.jpg" class="img-fluid" style="border-radius: 5px">
                    </div>
                    Except for those Services included in the "Effect Club" Loyalty Programme (See point 3 of this
                    F.A.Q, or Click here for more info), "Snaps" will be required to use Effect Group Services. 
                    </div> -->
                  </div>
              <?php endforeach ?>
          </div>
        </div>
      </div>
      
    </div>

    

    

    <footer class="bg-dark py-4 px-3 text-white">
      <div class="row">
        <div class="col-md-6 text-md-left text-center mb-md-0 mb-3">
          <a href="javascript:void(0)" data-toggle="modal" data-target="#modalTerms" data-backdrop="static" data-keyboard="false" class="text-white mr-3">TERM OF USE</a> | <a href="legal" class="text-white ml-3">LEGAL</a>
        </div>
       
        <div class="col-md-6 text-md-right text-center">
          <a href="javascript:void(0)" class="text-white mr-3"><i class="fas fa-rss mr-2"></i> RSS Feeds</a> | <span class="ml-3">Copyright © 2021 - Effect Group</span>
        </div>
      </div>
    </footer>

    <!-- Bootstrap-Cookie-Alert -->
    <div id="cookies" class="cookiealert p-4" role="alert" style="display: none">
      <div class="card card-body shadow">
            <h2>Do you like cookies?</h2>
            <h5>We use cookies to ensure you get the best experience on our website. (<a href="https://cookiesandyou.com/" target="_blank">Learn more</a>)</h5>
            <button class="btn btn-primary mt-4" onclick="accpetCookie()" style="width: 100px">
              I AGREE
            </button>
       
      </div>
    </div>
    <!-- END Bootstrap-Cookie-Alert -->
      
    <script src="//code.tidio.co/o0cylckokzypqxb4t2kajinc27qug63v.js" async></script>

    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/feather-icons/feather.min.js"></script>
    <script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="popup/videopopup.js"></script>
    <script src="assets/js/jquery.background-video.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>

    <script src="assets/js/dashforge.js"></script>

    <script>
        if (getCookie('accepted') === 'yes') {
            document.getElementById("cookies").style.display = "none";
        } else {
          setTimeout(function(){ document.getElementById("cookies").style.display = "block"; }, 5000);
        }

        // user clicks the confirmation -> set the 'yes' value to cookie and set 'accepted' as name
        function accpetCookie() {
            setCookie('accepted', 'yes', 100);
            document.getElementById("cookies").style.display = "none";
        }

        // code from :http://stackoverflow.com/a/4825695/191220
        // set cookie method
        function setCookie(c_name, value, exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = c_name + "=" + c_value;
        }

        // get cookie method   
        function getCookie(c_name) {
            var i, x, y, ARRcookies = document.cookie.split(";");
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                x = x.replace(/^\s+|\s+$/g, "");
                if (x == c_name) {
                    return unescape(y);
                }
            }
        }
    </script>
    <script>
      $(document).ready(function(){
        $(".preloader").delay(1200).fadeOut(); 

        


      });

      $(".links").click(function() {
        var target = $(this).attr("data-target");
        $('body').removeClass('navbar-nav-show');
          $('html, body').animate({
              scrollTop: $("#" + target).offset().top - 50
          }, 1000);
      });

      $(".choose").click(function(){
        var target = $(this).attr("data-target");


        $(".choose").removeClass("btn-brand-01 btn-link").addClass("btn-link");
        $(this).removeClass("btn-brand-01 btn-link").addClass("btn-brand-01");
        $(".choosen").hide();
        $("#" + target).fadeIn();
      });

      $(".screenshot").click(function(){
        var target = $(this).attr("data-target");


        $(".screenshot").removeClass("hovereds");
        $(this).addClass("hovereds");
        $(".fitur").hide();
        $("#" + target).fadeIn();
      });

      $('.use-case').hover(
        function(){
          var $this = $(this);
          $this.addClass("bordered");
          $this.find(".showin").hide();
          $this.find(".hiddin").show();
        },
        function(){
          var $this = $(this);
          $this.removeClass("bordered");
          $this.find(".hiddin").hide();
          $this.find(".showin").show();
        }
      );
      // $(window).scroll(function() {
      //   var x = $(".navbar").offset().top - 100;
      //   $(".sect").each(function(index) {
      //     var z = $(this).attr("id");
      //     if (x > $(this).offset().top && x <= $(this).offset().top + $(this).height()) {
      //       $('#S' + z).addClass("active");
      //     } else {
      //       $('#S.' + z).removeClass("active");
      //     }
      //   })
      // });

      $('.accordion').accordion({
          heightStyle: 'content',
          collapsible: true
      });


    </script>
    <!-- Fathom - beautiful, simple website analytics -->
<script src="https://cdn.usefathom.com/script.js" data-site="UAPNJREX" defer></script>
<!-- / Fathom -->
  </body>
</html>
