<?php 

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// print_r($actual_link); die;

?>
<header class="navbar navbar-header navbar-header-fixed bd-b-0 bg-primary">
  <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
  <div class="navbar-brand">
    <a href="./" class="df-logo text-white pl-md-5">
      <img src="img/headers.png" style="width: auto; height: 25px;">
    </a>
    
  </div><!-- navbar-brand -->
  <div id="navbarMenu" class="navbar-menu-wrapper">
    <div class="navbar-menu-header">
      <a href="./" class="df-logo">Effect Group</a>
      <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
    </div><!-- navbar-menu-header -->
    <ul class="nav navbar-menu">          
      <li id="Shome" class="nav-item links" data-target="home"><a href="javascript:void(0)" class="nav-link text-nav">Home</a></li>
      <li id="SaboutUs" class="nav-item links" data-target="aboutUs"><a href="javascript:void(0)" class="nav-link text-nav">What We Do</a></li>
      <li id="SuseCase" class="nav-item links" data-target="useCase"><a href="javascript:void(0)" class="nav-link text-nav">Use cases</a></li>
      <!-- <li id="SegPhone" class="nav-item links" data-target="egPhone"><a href="javascript:void(0)" class="nav-link text-nav">EG phone</a></li> -->
      <li class="nav-item"><a href="faq" class="nav-link text-nav">FAQ</a></li>
      <li class="nav-item"><a href="learning-center" class="nav-link text-nav">Learning Center</a></li>

      <li class="nav-item"><a href="effect-club" class="nav-link text-nav">Effect Club <span class="badge badge-danger ml-1" style="font-size: 6px; margin-top: -12px">NEW</span></a></li>
      <li class="nav-item"><a href="pricing" class="nav-link text-nav">Pricing</a></li>
      <!-- <li id="SrequestQuote" class="nav-item links" data-target="requestQuote"><a href="javascript:void(0)" class="nav-link text-nav">Request a Quote</a></li> -->
    </ul>
  </div><!-- navbar-menu-wrapper -->
  <div class="navbar-right">
    <a href="https://dashboard.effect.group/registration" target="_blank" class="btn btn-brand-01 btn-sm tx-md-14 px-2 py-1 tx-8 mr-2">SIGN UP</a>
    <a href="https://dashboard.effect.group/login" target="_blank" class="btn btn-light btn-sm tx-md-14 tx-8 px-2 py-1 text-primary">LOGIN</a>
  </div>
</header><!-- navbar -->