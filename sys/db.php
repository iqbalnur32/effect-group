<?php

class Database {
 
	var $host = "localhost";
        var $username = "root";
        var $password = "";
        var $database = "effect-group";
 
	function __construct(){
        $this->db = new mysqli($this->host, $this->username, $this->password,$this->database);
		if (mysqli_connect_errno()){
			echo "Connection database Failed : " . mysqli_connect_error();
		}
    }

    /* Site Url */
	public function site_url()
    {
    	$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		$config['base_url'] .= "://".$_SERVER['HTTP_HOST'];
		$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

		print_r($config['base_url']);
	}

	public function insert($table,$data){
		$fields = "(";
		$values = "(";
		$index  = 0;
		
		foreach ($data as $key => $val) {
		  	$fieldname = ($index < count($data)-1) ? $key.", " : $key. ")";
		  	$valuedata = ($index < count($data)-1) ? "'".$val."', "  : "'".$val."')";
		  	$fields .= $fieldname;
		  	$values .= $valuedata;
  
		  	$index++;
		}
  
	  	$query = $this->db->query("INSERT INTO ".$table." ".$fields." VALUES ".$values." ");
	  	return $query;
	}
  
	public function update($table_name, $fields, $where) {
	  	$query = '';  
	  	$condition = '';  
	  	foreach($fields as $key => $value){

		  	$query .= $key . "='".$value."', ";  
	  	}	  
	  	$query = substr($query, 0, -2);  
	  	foreach($where as $key => $value){  
		  	$condition .= $key . "='".$value."' AND ";  
	  	}  
	  	$condition = substr($condition, 0, -5);  
	 	$query = $this->db->query("UPDATE ".$table_name." SET ".$query." WHERE ".$condition."");  
	  	return $query;
	} 

	public function queryLogin($query){
		$data = $this->query = $this->db->query($query);
        return $data;
	}
	
    public function query($query){
        $this->query = $this->db->query($query);
        return $this;
	}

	public function getEachTable($table,$mandatory,$key){
		$this->each = $this->query("SELECT * FROM $table WHERE $mandatory = '$key'");
		// $data->fetch_assoc();
		return $this;
	}

	public function getTable($table,$key){
		$this->getTable = $this->query("SELECT * FROM $table ORDER BY $key DESC ");
		return $this;
	}
	
	public function getWhere($table,$condition,$key,$condition2){
		$this->where = $this->query("SELECT * FROM $table WHERE $condition ORDER BY $key $condition2");
		return $this;
	}

	// Query untuk sort by Price Dan Items Per page
	public function sortByFilter($table, $condition, $keywords, $key, $condition2)
	{
		$this->getSortFilter = $this->query("SELECT * FROM $table WHERE $condition LIKE '%$keywords%' ORDER BY $key $condition2");
		return $this;
	}

	// Query untuk sort by Items Per page
	public function sortByFilterLimit($table, $limit)
	{
		$this->getSortFilter = $this->query("SELECT * FROM $table LIMIT $limit");
		return $this;
	}

	/* Query untuk search sort data s*/
	public function getSearchSort($keyword)
	{
		$this->getSearchSort = $this->query("SELECT * FROM content WHERE title LIKE '%$keyword%' ");
		return $this;
	}

	public function rowCount(){
		$data = $this->query->num_rows;
		return $data;
	}

	public function rowObject(){

		while($data = $this->query->fetch_object()){
			$result[] = $data;
		}

		return empty($result) ? null : $result;
	}

	public function rowArray(){

		while($data = $this->query->fetch_assoc()){
			$result[] = $data;
		}
		return empty($result) ? null : $result;
	}
	public function orderBy($condition1,$condition2){
		

	}

	public function result(){
		return $this->query->fetch_all();
	}

} 
?>
