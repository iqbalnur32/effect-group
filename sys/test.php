<?php 

include "db.php";

$db = new Database();

if (isset($_GET['q'])) {
	/* Sort Input Search */
	$keyword = trim($_GET['q']);
	$query = $db->getSearchSort($keyword)->rowObject();
	echo json_encode($query);

}elseif (isset($_GET['price'])) {
	/* Sort Price */
	$price = trim($_GET['price']);
	$query_price = $db->sortByFilter('content', 'price', $price, 'price', 'DESC')->rowObject();
	echo json_encode($query_price);

}elseif (isset($_GET['limit'])) {
	/* Sort Limit */
	$limit = trim($_GET['limit']);
	$query_price = $db->sortByFilterLimit('content', $limit)->rowObject();
	echo json_encode($query_price);
	
}else{
	echo json_encode('gagal');
	// http://localhost/website-chandra/sys/test.php?q=kemeja
}



?>