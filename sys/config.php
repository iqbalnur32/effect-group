<?php 

// include 'db.php';
session_start();
// DEFINE("WEB", "https://btrapparels.com/"); // SET WEBSITE ADDRESS
DEFINE("WEB", "https://202.145.0.107/apps/effect-group/"); // SET WEBSITE ADDRESS

class Config extends Database
{
	/*private $db;

	public function __construct()
	{
		$this->db = new Database();
	}*/

	public function Redirect($uri, $statusCode = true)
    {
    	header('Location: '. $uri,  $statusCode);
    }

	public function uuid($trim = false) 
	{
		$format = ($trim == false) ? '%04x%04x-%04x-%04x-%04x-%04x%04x%04x' : '%04x%04x%04x%04x%04x%04x%04x%04x';
		return sprintf($format,
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),
			mt_rand(0, 0xffff),
			mt_rand(0, 0x0fff) | 0x4000,
			mt_rand(0, 0x3fff) | 0x8000,
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

	/* Random String */
	public function randomString($length = 6) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	/* Site Url */
	public function site_url()
    {
    	$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		$config['base_url'] .= "://".$_SERVER['HTTP_HOST'];
		$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

		print_r($config['base_url']);
	}

	/* TotalResultPric */
	public function totalResultPrice()
	{
	    $email = $_SESSION['email'];
		$sql = $this->query("SELECT SUM(total_result) as JumlahKeseluruhan FROM cart WHERE email='$email' ")->rowObject();
		echo $sql[0]->JumlahKeseluruhan;
	}

	/* Check QTY Jika Ada Dia Update */
	public function checkQty($id_cart, $qty, $total_result)
	{
		$sql = $this->query("SELECT * FROM cart WHERE id_cart = '$id_cart' ")->rowObject();
		if ($sql) {
			$updateCart = $this->query("UPDATE `cart` set qty = '$qty', total_result = '$total_result' WHERE id_cart = '$id_cart'");
			return true;
		}else{
			return false;
		}
	}

	/* Paid Payment */
	public function PaidPayment($data)
	{	
		$email = $_SESSION['email']; 
		$id_order = $data['id_order'];
		$result_payment = $this->query("DELETE FROM `cart` WHERE email= '$email' ");
		// var_dump($result_payment); die();
		if ($result_payment == true) {
			$this->query("UPDATE `order_history` SET status_pembayaran='1' WHERE email = '$email' AND id_order='$id_order'");
			echo "<script>
				setTimeout(() => {
					window.location.href= 'order-history'
				}, 1000)
			</script>";
		}else{
			echo "<script>alert('Gagal Paid Payment')</script>";
		}
	}

	/* Chckput Payment */
	public function CheckoutPayment($data)
	{
		if (!empty($data['alamat'])) {
		  //  var_dump(preg_replace('/\D/','',$data['subtotalPembayaran'])); die;
			$product = $data['product'];
			$product_data = array();
			foreach ($product as $key) {
				$product_data[] = $key;
			}

			$id_content = $data['id_content'];
			$id_content_data = array();
			foreach ($id_content as $key) {
				$id_content_data[] = $key;
			}

			$qty = $data['qty'];
			$qty_data = array();
			foreach ($qty as $key) {
				$qty_data[] = $key;
			}
			
			$id_order = $this->uuid();

			$checkout = array(
				'id_order' 				=> $id_order, 
				'id_content' 			=> json_encode($id_content_data), 
				'qty' 					=> json_encode($qty_data), 
				'product' 				=> json_encode($product_data), 
				'no_order' 				=> $this->randomString(), 
				'status'				=> '0',
				'status_pembayaran'		=> '0',
				'alamat' 				=> $data['alamat'], 
				'total' 				=> $data['subtotalPembayaran'], 
				'email' 				=> $_SESSION['email'], 
				'note' 		    		=> $data['note'], 
				'payment' 				=> $data['payment'], 
				'created_date' 			=> date('Y-m-d H:i:s'), 
				'created_by' 			=> $_SESSION['email'], 
				'is_delete' 			=> '0', 
			);
			
			$sql = $this->insert('order_history', $checkout);
			if($sql == true){
			 echo "<script>
				setTimeout(() => {
					window.location.href= 'checkout?detail-order=".$id_order."'
				}, 1000)
			</script>";   
			}else{
			    echo "sdsa";
			}
		}else{
			echo "<script>alert('anda belum isi alamat jancok')</script>";
		}
	}
	
	/* Get Each Product */
	public function getEachProduct($key){
		$sql = $this->query("SELECT * FROM content WHERE code_product = '$key' ")->rowObject();
		return $sql;
	}

	public function getWhereEach($table, $condition, $key){
		$sql = $this->query("SELECT * FROM $table WHERE $condition = '$key' ")->rowObject();
		return $sql;
	}

	/* Coupun Submit */
	public function submitCoupun($data)
	{
		$code = $data['coupon_code'];
		$couupun = $this->getEachTable("coupun_code", "code_coupun", $code);
		$couupun = $this->rowObject();
		
		return $couupun;
	}

	/* Check Cart */
	public function checkCart($email, $no_product)
	{
		$sql = $this->query("SELECT * FROM cart WHERE email = '$email' AND no_product = '$no_product' ")->rowObject();
		if ($sql) {
			return true;
		}else{
			return false;
		}
	}

	/* Add To Cart */
	public function addTocart($id, $no_product, $nama_product, $email, $qty, $id_content, $total_result)
	{
		$totalQty = $qty * $total_result;
		$cart_data = array(
			'id_cart' 		=> $id, 
			'no_product' 	=> $no_product,
			'total_result'  => $totalQty,
			'nama_product' 	=> $nama_product, 
			'email' 		=> $email, 
			'qty' 			=> $qty, 
			'id_content' 	=> $id_content, 
			'created_date' 	=> date('Y-m-d H:i:s') 
		);
		$sql = $this->insert('cart', $cart_data);
		if ($sql) {
			echo "berhasil";
		}else{
			echo "gagal";
		}
	}

	/* Get Product */
	public function getProduct($no_product)
	{
		$sql = $this->query("SELECT * FROM cart WHERE no_product='$no_product' ")->rowObject();
		return $sql;
	}

	/* Update To Cart */
	public function updateToCart($id, $no_product, $nama_product, $email, $qty, $id_content, $total_result)
	{
		$getProduct = $this->getProduct($no_product);
		$currentQty = $getProduct[0]->qty;
		$sql = $this->query("UPDATE cart SET qty = '$currentQty' + '$qty', total_result = '$total_result' WHERE email = '$email' AND no_product = '$no_product' AND id_content = '$id_content' ");
		if ($sql) {
			echo "berhasil";
		}else{
			echo "gagal";
		}
	}

	/* Remove Cart */
	public function removeCart($no_product)
	{
		$sql = $this->query("DELETE FROM cart WHERE no_product = '$no_product' ");
		return $sql;
		
	}

	/* Count Cart */
	public function countCart($email)
	{
		$sql = $this->query("SELECT * FROM cart WHERE email='$email'")->rowCount();
		echo $sql;
	}

	/* Get Cart */
	public function getProductCart($email)
	{
		$sql = $this->query("SELECT * FROM cart WHERE email='$email'")->rowObject();
		return $sql;
	}

	/* Login */
	public function getLogin($email)
	{
		$sql = $this->query("SELECT * FROM user WHERE email = '$email' LIMIT 1")->rowObject();
		return $sql;
	}

	/* Get Logo Image */
	public function logoImage()
	{
		$logo_img = $this->query("SELECT * FROM logo")->rowObject();
		return $logo_img;
	}

	/* Filter Pencarian */
	public function FunctionName()
	{
		# code...
	}

	/* filter price */
	public function FilterPrice($data, $limit_start, $limit)
	{
		if ($data['filter_price']) {
			$amount_min = $data['amount_min'];
			$amount_max = $data['amount_max'];

			$sql = "SELECT * FROM content ";
			if($data['amount_min'] != "0") $sql .= "WHERE price BETWEEN  '$amount_min' AND ";
			if($data['amount_max'] != "0") $sql .= " '$amount_max' ";
			$sql .= "ORDER BY price ASC LIMIT $limit_start, $limit";
			//var_dump($sql); die;
			$result = $this->query($sql);
    		$result = $this->rowObject();
			return $result;		
 			
		}
	}

	/* Profile Update */
	public function ProfileUpdate($data)
	{
		$id_user = $data['id_user'];
		$where = array(
			'id_user' => $id_user
		);
		// print_r($data); die();
		if ($data['password']) {
			$update = array(
				'password' 	=> $data['password'], 
				'full_name' => $data['full_name'], 
				'username' 	=> $data['username'], 
				'email' 	=> $data['email'], 
				'alamat' 	=> $data['alamat'],  
				'no_telp' 	=> $data['no_telp'],  
			);
			$this->update('user', $update, $where);
			return $this->Redirect('profile', 200);
		}else{
			$update = array(
				'full_name' => $data['full_name'], 
				'username' 	=> $data['username'], 
				'email' 	=> $data['email'], 
				'alamat' 	=> $data['alamat'],  
				'no_telp' 	=> $data['no_telp'],   
			);
			$this->update('user', $update, $where);
			return $this->Redirect('profile', 200);
		}
	}

	/* User Login */
	public function userLogin($data)
	{

		$check_login = $this->getLogin($data['email']);
		if ($check_login) {
			if (password_verify($data['password'], $check_login[0]->password)) {
				$session = array(
					'id_user' 	=> $check_login[0]->id_user, 
					'id_role' 	=> $check_login[0]->id_role, 
					'full_name' => $check_login[0]->full_name, 
					'email' 	=> $check_login[0]->email,    
					'username' 	=> $check_login[0]->username,  
					'isLogin' 	=> true,  
				);

				$_SESSION = $session;

				/*if ($check_login[0]->id_role == '2') {
					
				}*/
				header("Location: ".WEB."index");
				exit;
			}else{
				echo "<script>alert('Username dan password tidak sesuai')</script>";
			}
		}else{
			echo "<script>alert('Username tidak terdaftar')</script>";
		}
	}

	public function insertRegister($data)
	{	
		$result = array(
			'id_user' => $this->uuid(), 
			'id_role' => 2, 
			'full_name' => $data['full_name'], 
			'email' => $data['email'], 
			'username' => $data['username'], 
			'password' => password_hash($data['password'], PASSWORD_DEFAULT), 
			'level' => 'users', 
			'is_active' => 1, 
			'permission_create' => 0, 
			'permission_read' => 0, 
			'permission_update' => 0, 
			'permission_delete' => 0,  
			'is_delete' => '0', 
		);
		// print_r($result); die();
		$sql = $this->insert('user', $result);
		if ($sql) {
			header("Location: ".WEB."login-register");
			exit;
		}else{
			header("Location: ".WEB."login-register");
			exit;
		}
	}
}

?>