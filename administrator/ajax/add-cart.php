<?php
include '../sys/db.php';
include '../sys/config.php';
$config = new Config();



if ($_SERVER["REQUEST_METHOD"] === "POST") {
    
    $id = $config->uuid();
    $id_content = $_POST['id_content'];
    $total_result = $_POST['total_result'];
    $no_product = $_POST['code_product'];
    $nama_product = $_POST['title'];
    $email = $_POST['email'];
    $qty = $_POST['qty'];
    $checkCart = $config->checkCart($email, $no_product);
    if ($checkCart == true) {
    	$config->updateToCart($id, $no_product, $nama_product, $email, $qty, $id_content, $total_result);
    }else{
    	$config->addTocart($id, $no_product, $nama_product, $email, $qty, $id_content, $total_result);
    }
}

?>