<?php 
include '../sys/db.php';
include '../sys/config.php';
$c = new Config();

?>
<?php if (empty($_SESSION)) { ?>
<script type="text/javascript">alert('Anda Belum Login')</script>
<?php }else{ ?>
<?php 	
    $cart_item = $c->getProductCart($_SESSION['email']);
    // $profile_user = $c->getEachTable('user', 'id_user', $_SESSION['id_user'])->rowObject();
?>

<div class="row contacts-block">
	<div class="col-12 col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="font-weight: bold;">Remove</th>
					<th style="font-weight: bold;">Images</th>
					<th style="font-weight: bold;">Product</th>
					<th style="font-weight: bold;">Unit Price</th>
					<th style="font-weight: bold;">Quantity</th>
					<th style="font-weight: bold;">Total</th>
				</tr>
			</thead>
			<tbody class="">
				<?php if (empty($cart_item)): ?>
					<a href="shop" class="btn btn-xl btn-warning btn-block">Upss Barang Anda Kosong, Kembali Belanja</a>
				<?php endif ?>
				<br>
				<br>
				<?php foreach ((array) $cart_item as $key) { ?>
					<?php
				         $img_prodcut = $c->getEachProduct($key->no_product);
				         $parsing_img = json_decode($img_prodcut[0]->img_content);
				         // var_dump($parsing_img[0]);die;
				        //  echo "<pre>"; 
				         // var_dump($img_prodcut[0]->img_content);
				    ?>
					<tr>
						<td><a id="remove_cart" data-no_product="<?= $key->no_product ?>"><i class="fa fa-trash-o"></i></a></td>
						<td><img src="<?php echo WEB ?>administrator/assets/img/<?= $parsing_img[0] ?>" alt="Uren's Cart Thumbnail" style="height: 200px; width: 200px;"></td>
						<td><?= $key->nama_product ?></td>
						<td>Rp <?= number_format($img_prodcut[0]->price) ?></td>
						<td>
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-btn">
									<button class="dec<?= $key->id_cart ?> btn btn-sm btn-warning" data-id_cart="<?= $key->id_cart ?>" data-price="<?= $img_prodcut[0]->price ?>"><i class="fa fa-minus"></i></button>
									</span>
									<input type="text" name="qty" id="qty<?= $key->id_cart ?>" class="qty form-control input-number" value="<?= $key->qty ?>">
									<span class="input-group-btn">
									<button class="inc<?= $key->id_cart ?> btn btn-sm btn-warning" data-id_cart="<?= $key->id_cart ?>" data-price="<?= $img_prodcut[0]->price ?>"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
						</td>
						<input type="hidden" id="price<?= $key->id_cart ?>" value="<?= $img_prodcut[0]->price ?>" >
						<!-- <td id="total<?= $key->id_cart ?>">Rp <?= number_format($key->total_result)  ?></td> -->
						<td id="total<?= $key->id_cart ?>"></td>
					</tr>

					<script type="text/javascript">
						$(document).ready(function() {
							/* Convert To Number */
					        function numberWithCommas(x) {
					          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					        }

							var kontol = $('#price<?= $key->id_cart ?>').val()
							var qty = $('#qty<?= $key->id_cart ?>').val()
							var jembut = qty * kontol
							$('#total<?= $key->id_cart ?>').html(numberWithCommas('Rp. ' +  jembut))
						})
					</script>
				<? } ?>
			</tbody>
		</table>
	</div>
</div>
<br>
<?php } ?>

<!-- /* Setelah chekout data cart yang berisi harga, discount akan masuk / di insert ke dalam database order untuk record data nya */ -->
<!-- /* Kondisikan jika sessionnya kosong dia tidak bisa checkout */ -->
<!-- Membuat QTY Menjadi Dinamsi Bray  => udah bray--> 
<!-- Membuat Total Product Menjadi Dinamsi Bray  => Belum bray--> 
<!-- Membuat Total Dynamic Product Total Lewat Clietn Side No Refresh pokoknya cok -->

