$(function(){
	var base_url = 'http://localhost/'

	/*  ======  Data Master Category =====  */
	$(document).on('click', '#modal_category', function() {

		var id_category = $(this).data('id_category');	
		// console.log(id_category);
		$.ajax({
			url: base_url + 'admin-toko/action/Master/editCategory/' + id_category,
			type: 'GET',	
			dataType: 'JSON',	
			success: function(data){
				$('#category').val(data.name_category);
				$('#id_category').val(data.id_category);
			},
			error: function(err){
				swal({
					title: 'Oooppsss',
					type: 'error',
					text: err,
					timer: 10000,
					showConfirmButton: true
				});
			}
		})
		
		// var category = $(this).data('name_category');
		// $('#category').val(category);
		// $('#id_category').val(id_category);
		// $('#modaCategory').modal('show');
	})

	/*$('#submit').click(function(e) {
		e.preventDefault();
		var formData = {
			name_category:	$('#category').val(),
			id_category:	$('#id_category').val(),
		}
		var id_category = $('#id_category').val()

		var type = "POST";
		var dataType = 'JSON';

		$.ajax({
			url: base_url + 'admin-toko/action/Master/editCategoryProcess/' + id_category,
			type: type,
			data: formData,
			dataType: dataType,
			success: function(data){
				swal({
					title: 'Yeeaayy',
					type: 'success',
					text: 'Menambahkan Data Berhasil',
					timer: 10000,
					showConfirmButton: true
				}, function() {
					location.reload();
				})	
			},
			error: function(err){
				swal({
					title: 'Oooppsss',
					type: 'error',
					text: err,
					timer: 10000,
					showConfirmButton: true
				});
			}
		})

	})*/

	/*  ====== End Data Master Category =====  */

	/*  ======  Data Master Banner =====  */
	$(document).on('click', '#banner_category', function() {
		var id_banner = $(this).data('id_banner');
		var nama_banner = $(this).data('nama_banner');
		var img_banner = $(this).data('img_banner');
		$('#id_banner').val(id_banner)
		$('#nama_banner').val(nama_banner)
		$('#img').attr("src", "/admin-toko/assets/img/"+img_banner); 
		console.log(nama_banner);
	})
	/*  ====== End Data Master Banner =====  */
	
	/*  ====== Data Master Content =====  */
	/*$(document).on('click', '#modal_content', function() {
		var id_content = $(this).data('id_content')
		$.ajax({
			url: base_url + 'admin-toko/action/Master/editContent/' + id_content,
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				console.log(data);
				// $('#id_category').find(":selected").text(data.name_category);
				$('#id_content').val(id_content)
				$('#title').val(data.title)
				$('#price').val(data.price)
				$('#link_1').val(data.link_1)
				$('#link_2').val(data.link_2)
				$('#description').val(data.description)
				$('#img').attr("src", "/admin-toko/assets/img/"+data.img_content);

			},
			error: function(err){
				console.log(err);
			}
		})
	})*/

	/*  ====== End Data Master Content =====  */

	/*  ====== Order History =====  */

		/* Jqeury Change Data Edit */
		$('#id_content').on('change',function(){
	        var optionText = $("#id_content option:selected").text();
	        var data = optionText.split(" ")

	        $('#product').val(data[0])
	    });

		/* Jqeury Change Data Edit */
	    $('#edit_content').on('change',function(){
	        var optionText = $("#edit_content option:selected").text();
	        var data = optionText.split(" ")

	        $('#edit_product').val(data[0])
	    });

	    /* Edit Order */
	    /*$(document).on('click', '#edit_order', function() {
	    	var id_order = $(this).data('id_order')
	    	$.ajax({
				url: base_url + 'admin-toko/action/Master/editOrderHistory/' + id_order,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					console.log(data);
					$('#id_order').val(id_order)
					$('#no_order').val(data.no_order)
					// $('#edit_product').val(data.product)
					// $('#status').find(":selected").text(da);
				},
				error: function(err){
					alert('Erorr !, Silahkan Refresh')
				}
			})
	    })
*/	/*  ====== EndOrder History =====  */

})