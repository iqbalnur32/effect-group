<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('MasterModel','model',TRUE);
		if (!$this->session->userdata('LOGGED_TOKEN')) {
			redirect('Auth');
		}

	}
	public $mainTitle = 'Toko';
	public $data = [
		// TEMPLATE
		'navbar' => 'backend/inc/navbar',
		'sidebar' => 'backend/inc/sidebar',
		'title' => 'Toko',
		'content' => 'backend/content/dashboard',
		'contentTitle' => '',
		'contentSubtitle' => '',
		// SIDEBAR MENU Untuk Active
		'dashboardActive' => '',
		'orderHistoryActive' => '',
		'menuOpenInput' => '',
		'menuOpenData' => '',
		'menuOpenGrafik' => '',
		'menuOpenSection' => '',
		'menuActiveSection' => '',
		'menuActive' => '' ,
		'menuActiveData'=> '',
		'menuActiveGrafik'=> '',
		'inputLogoActive' => '',
		'inputAboutHome' => '',
		'inputHomeBanner' => '',
		'inputCase1' => '',
		'inputCase2' => '',
		'inputXIIActive' => '',
		'dataXActive' => '',
		'dataBanner' => '',
		'dataContent' => '',
		'dataLogo' => '',
		'dataMenuCase2' => '',
		'faq' => '',
		'grafikXIActive' => '',
		'grafikXIIActive' =>'',
		'inputSectionHome' => '',
		'editDiscountActive' => '',
		'inputSection1' => '',
		'inputSection2' => '',
		'inputEffetGroup' => '',
		'inputFooter' => ''

	];

	public static function setMessage($title = null, $type = null, $message = null) {
		return $this->session->set_flashdata([
			'MESSAGE_type' => $type,
			'MESSAGE_title' => $title,
			'MESSAGE_message' => $message
		]);
	}

	public function imageConf() {
		$config['upload_path'] = './assets/img';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|jfif';
		$config['max_size']  = '20000';
		$config['overwrite']  = true;
		$config['encrypt_name']  = true;
		return $this->load->library('upload', $config);
	}
	
	public function randomString($length = 4) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	public function uuid($trim = false) 
	{
		
		$format = ($trim == false) ? '%04x%04x-%04x-%04x-%04x-%04x%04x%04x' : '%04x%04x%04x%04x%04x%04x%04x%04x';
    	
		return sprintf($format,

			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

}
