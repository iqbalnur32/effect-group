<?php
date_default_timezone_set('Asia/Jakarta');

class Master extends MY_Controller{

	public function index(){
    	redirect('pages');
  	}

  	/* Set Message Notif */
  	public function setMessages($title = null, $type = null, $message = null) {
		return $this->session->set_flashdata([
			'MESSAGE_type' => $type,
			'MESSAGE_title' => $title,
			'MESSAGE_message' => $message
		]);
	}

	/* ======= Navigation ======= */
	public function inputNavigationProcess()
	{
		if ($this->input->post()) {
			$data_navigation = array();
			for($i=0; $i < count($this->input->post('title_navigation')); $i++){

				if($this->input->post('title_navigation')[$i] != ""){
					$title_navigation	 	 = $this->input->post('title_navigation')[$i];
					$link_navigation	 	 = $this->input->post('link_navigation')[$i] != '' ? $this->input->post('link_navigation')[$i] : '';
					
					$data = array(
						'id_nav' 			=> $this->uuid(),
						'title_navigation'  => $title_navigation,
						'link_navigation'   => $link_navigation,
						'created_by' 		=> $this->session->userdata('LOGGED_USERNAME'),
						'created_date' 		=> date('Y-m-d H:i:s'),
					);
					
					$result = $this->model->insert('navigation',$data);
				}
			}

			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputNavigation');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputNavigation');
			}
		}
	}

	public function editNavigationProcess()
	{
		if ($this->input->post()) {
			$id = $this->input->post('id_nav',TRUE);	// get id nav//
			$data = array(
				'title_navigation'  => $this->input->post('title_navigation'),
				'link_navigation'   => $this->input->post('link_navigation'),
				'updated_by' 		=> $this->session->userdata('LOGGED_USERNAME'),
				'updated_date' 		=> date('Y-m-d H:i:s')
			);
			$this->db->where('id_nav', $id);
			$result = $this->db->update('navigation', $data);

			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputNavigation');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputNavigation');
			}
		}
	}

	public function deleteNavigationProcess($id)
	{
		$this->model->delete('navigation', 'id_nav', $id);
		$this->setMessages('Yeeaayy','success','Delete Data Berhasil');
		redirect('pages/inputNavigation');
	}
	/* ======= End Navigation ======= */

	/* Input Banner */
	public function inputBaner(){
		if ($this->input->post()) {
			$endpoint = $this->input->post('endpoint_url') != "" ? base_url() . $this->input->post('endpoint_url') : base_url();
			$banner = array(
				'id_banner' 		=> $this->uuid(),
				// 'img_banner' => json_encode($data),
				'url_endpoint'	    => $this->input->post('endpoint_url'),
				'nama_banner' 		=> $this->input->post('nama_banner'),
				'description' 		=> $this->input->post('description'),
			);
			// var_dump($banner); die();
			$result = $this->model->insert('banner', $banner);
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputbanner');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputbanner');
			}
		}
	}

	/* Edit Banner Process */
	public function editBannerProcess()
	{
		$id = $this->input->post('id_banner',TRUE);	// get id banner//
		$banner = array(
			'nama_banner' => $this->input->post('nama_banner'),
			'description' => $this->input->post('description'),
		);
		$this->db->where('id_banner', $id);
		$result = $this->db->update('banner', $banner);

		if ($result === true) {
			$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
			redirect('pages/MasterBanner');
		}else{
			$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
			redirect('pages/MasterBanner');
		}
	}

	/* Edit Banner */
	public function editBanner($id)
	{
		// $data = $this->model->getEditTable('content', 'id_content', $id); // Data Content Lasgung Tanpa Join
		$data = $this->db->from('banner')
				->where('id_banner', $id)
				->get()
				->row();

		if ($data != null) {
			echo json_encode($data);
		}else{
			echo json_encode("Data Null");
		}
	}

	/* Delete Banner */
	public function deleteBannerProcess($id)
	{
		$this->model->delete('banner', 'id_banner', $id);
		$this->setMessages('Yeeaayy','success','Delete Data Berhasil');
		redirect('pages/MasterBanner');
	}
	
	/* Input Section Home */
	public function inputSectionHome(){
	    if($this->input->post()){
	        $data = array(
				'id_home' 			=> $this->uuid(),
				'title_home' 		=> $this->input->post('title_section'),
				'icons_home' 		=> $this->input->post('icons_image'),
				'image_home' 		=> $this->input->post('image_section'),
				'description_home' 	=> $this->input->post('description'),
				'created_by' 		=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 		=> date('Y-m-d H:i:s'),
			);
			// var_dump($data); die();
			$result = $this->model->insert('section_home',$data);
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputsectionhome');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputsectionhome');
			}
	    }else{
	        $this->setMessages('Oooppsss','error','Form Harus Terisi Semua');
			redirect('pages/inputsectionhome');
	    }
	}
	   
	/* Edit Section Home Process */
	public function editSectionHomeProcess(){
	    if($this->input->post()){
	        $id = $this->input->post('id_home',TRUE);
	        
			$data = array(
				'id_home' 			=> $this->uuid(),
				'title_home' 		=> $this->input->post('title_section'),
				'icons_home' 		=> $this->input->post('icons_image'),
				'image_home' 		=> $this->input->post('image_section'),
				'description_home' 	=> $this->input->post('description'),
				'created_by' 		=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 		=> date('Y-m-d H:i:s'),
			);
			
			// Query Sql
			$this->db->where('id_home', $id);
			$result = $this->db->update('section_home', $data);
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Update Data Success !!');
				redirect('pages/MasterSectionHome');
			}else{
				$this->setMessages('Oooppsss','error','Update Data Failed !!');
				redirect('pages/MasterSectionHome');
			}
	    }else{
	        $this->setMessages('Oooppsss','error','Form Harus Terisi Semua');
			redirect('pages/MasterSectionHome');
	    }
	}
	
	/* Delete Section Home */
	public function deleteSectionHomeProcess($id)
	{
		$this->model->delete('section_home', 'id_home', $id);
		$this->setMessages('Yeeaayy','success','Delete Data Success !!');
		redirect('pages/MasterSectionHome');
		
	}

	/* ====== Input Section 2 =======*/
	public function inputSection2()
	{
		if ($this->input->post()) {
			
			$data_image = array();
			$files = $_FILES;
		    for($i = 0; $i < count($_FILES['img_section2']['name']) ; $i++)
		    {           
		        $_FILES['img_section2']['name']		= 	$files['img_section2']['name'][$i];
		        $_FILES['img_section2']['type']		= 	$files['img_section2']['type'][$i];
		        $_FILES['img_section2']['tmp_name']	= 	$files['img_section2']['tmp_name'][$i];
		        $_FILES['img_section2']['error']	= 	$files['img_section2']['error'][$i];
		        $_FILES['img_section2']['size']		= 	$files['img_section2']['size'][$i];    
		       	$this->imageConf();
		       	if (!$this->upload->do_upload('img_section2')){
		       		$this->setMessages('Oooppsss','error',strip_tags($this->upload->display_errors()));
					redirect('pages/sectionEffectGroup2');
		       	}else{
		       		$gambar = $this->upload->data('file_name');
					$data_image[] = $gambar;
		       	}
		    }
			
			$data = array(
		    	'id_section_2' 	=> $this->uuid(),
				'img_content' 	=> json_encode($data_image),
				'title'      	=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				// 'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				// 'created_date' 	=> date('Y-m-d H:i:s'),
			);
			$result = $this->model->insert('section_effect_2', $data);
			// var_dump($result); die;
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/sectionEffectGroup2');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/sectionEffectGroup2');
			}

		}
	}

	public function editSectionProcess2()
	{
		$id_section_2 = $this->input->post('id_section_2');
		$data_image = array();
		// print_r($_FILES['img_section2']['name'][0]); die;
		if (!empty($_FILES['img_section2']['name'][0])) {
			$files = $_FILES;
			$get_image = $this->db->select('*')->where('id_section_2', $id)->from('section_effect_2')->get()->row();
			$labels = array();

			foreach ((object)json_decode($get_image->img_content) as $key => $value) {
				$image_path = 'assets/img/';
				$labels[] = [
					'img' => unlink($image_path.$value)
				];
			}
		    
		    for($i = 0; $i < count($_FILES['img_section2']['name']); $i++)
		    {           
		        $_FILES['img_section2']['name']		= 	$files['img_section2']['name'][$i];
		        $_FILES['img_section2']['type']		= 	$files['img_section2']['type'][$i];
		        $_FILES['img_section2']['tmp_name']	= 	$files['img_section2']['tmp_name'][$i];
		        $_FILES['img_section2']['error']		= 	$files['img_section2']['error'][$i];
		        $_FILES['img_section2']['size']		= 	$files['img_section2']['size'][$i];    
		       	$this->imageConf();
		       	if (!$this->upload->do_upload('img_section2')){
		       		$this->setMessages('Oooppsss','error',strip_tags($this->upload->display_errors()));
					redirect('pages/sectionEffectGroup2');
		       	}else{
		       		$img_faq_data = $this->upload->data('file_name');
					$data_image[] = $img_faq_data;
		       	}
		    }

		    $data = array(
				'title'			  => $this->input->post('title'), 
				'img_content' 	  => json_encode($data_image),  
				'description'	  => $this->input->post('description'), 
			);

			$this->db->where('id_section_2', $id_section_2);
			$result = $this->db->update('section_effect_2', $data);

			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/sectionEffectGroup2');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/sectionEffectGroup2');
			}

		}else{

			$data = array(
				'title'			  => $this->input->post('title'),   
				'description'	  => $this->input->post('description'), 
			);

			$this->db->where('id_section_2', $id_section_2);
			$result = $this->db->update('section_effect_2', $data);

			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/sectionEffectGroup2');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/sectionEffectGroup2');
			}
		}
	}

	public function deleteSectionProcess2($id)
	{
		$data = $this->db->select('*')->where('id_section_2', $id)->from('section_effect_2')->get()->row();
		$labels = array();
		foreach ((array) json_decode($data->img_content) as $key => $value) {
			$image_path = 'assets/img/';
			$labels[] = [
				'img' => unlink($image_path.$value)
			];
		}

		if (!empty($labels)) {
			$this->model->delete('section_effect_2', 'id_section_2', $id);
            $this->setMessages('Yeeaayy','success','Delete Data Berhasil');
			redirect('pages/sectionEffectGroup2');
		}else{
			$this->model->delete('section_effect_2', 'id_section_2', $id);
			$this->setMessages('Oooppsss','error','Gagal Delete, Pastikan Data Anda Benar Benar Ada');
			redirect('pages/sectionEffectGroup2');
		}
	}
	/* ====== End Input Section 2 =======*/

	/*  Create About US Home Page */
	public function inputAboutUsHome(){
		if ($this->input->post()) {
			$data = array(
				'id_about' => $this->uuid(),
				'title_about' => $this->input->post('title_about'),
				'description' => $this->input->post('description'),
				'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 	=> date('Y-m-d H:i:s'),
			);
			// var_dump($data); die();
			$result = $this->model->insert('about_us_home',$data);
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputAboutHome');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputAboutHome');
			}
		}
	}

	/* Edi About Us Home Page */
	public function editHomePage($id)
	{
		$data = $this->model->getEditTable('about_us_home', 'id_about', $id);

		if ($data != null) {
			echo json_encode($data);
		}else{
			echo json_encode("Data Null");
		}

	}

	/* Edit About Us Home Page */
	public function editAboutUsHomePage()
	{
		if ($this->input->post()) {
			$id = $this->input->post('id_about',TRUE);

			$data = array(
				'id_about' => $this->uuid(),
				'title_about' => $this->input->post('title_about'),
				'description' => $this->input->post('description'),
				'updated_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'updated_date' 	=> date('Y-m-d H:i:s'),
			);

			// Query Sql
			$this->db->where('id_about', $id);
			$result = $this->db->update('about_us_home', $data);
			
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/MasterCategory');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/MasterCategory');
			}
		}
	}

	/* Delete About Us Home Pahe */
	public function deleteAboutUsHome($id)
	{
		$this->model->delete('banner', 'id_banner', $id);
		$this->setMessages('Yeeaayy','success','Delete Data Berhasil');
		redirect('pages/MasterBanner');
		
	}

	/* ======= Input Case 1 ====== */
	public function inputCase1()
	{
		if ($this->input->post()) {
			$data = array(
				'id_use_case_1' => $this->uuid(),
				'title' => $this->input->post('title_case1'),
				'description' => $this->input->post('description'),
				'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 	=> date('Y-m-d H:i:s'),
			);
			// var_dump($data); die();
			$result = $this->model->insert('use_case_1',$data);
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputUseCase1');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputUseCase1');
			}
		}
	}

	public function editUseCase1()
	{
		if ($this->input->post()) {
			$id = $this->input->post('id_use_case_1',TRUE);
			$data = array(
				'id_use_case_1' => $this->uuid(),
				'title' => $this->input->post('title_case1'),
				'description' => $this->input->post('description'),
				'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 	=> date('Y-m-d H:i:s'),
			);

			// Query Sql
			$this->db->where('id_use_case_1', $id);
			$result = $this->db->update('use_case_1', $data);
			
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/MasterCase1');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/MasterCase1');
			}
		}
	}

	public function deleteUseCase1($id)
	{
		$this->model->delete('use_case_1', 'id_use_case_1', $id);
		$this->setMessages('Yeeaayy','success','Delete Data Berhasil');
		redirect('pages/MasterCase1');
	}

	/* ======= End Input Case 1 ====== */

	/* ======= Input Case 2 ====== */
	public function inputCase2()
	{
		if ($this->input->post()) {
			$data = array(
				'id_use_case_2' => $this->uuid(),
				'title' => $this->input->post('title_case2'),
				'description' => $this->input->post('description'),
				'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 	=> date('Y-m-d H:i:s'),
			);
			// var_dump($data); die();
			$result = $this->model->insert('use_case_2',$data);
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/inputUseCase2');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/inputUseCase2');
			}
		}
	}

	public function editUseCase2()
	{
		if ($this->input->post()) {
			$id = $this->input->post('id_use_case_2',TRUE);
			$data = array(
				'title' => $this->input->post('title_case2'),
				'description' => $this->input->post('description'),
				'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 	=> date('Y-m-d H:i:s'),
			);

			// Query Sql
			$this->db->where('id_use_case_2', $id);
			$result = $this->db->update('use_case_2', $data);
			
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/MasterCase2');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/MasterCase2');
			}
		}
	}

	public function deleteUseCase2($id)
	{
		$this->model->delete('use_case_2', 'id_use_case_2', $id);
		$this->setMessages('Yeeaayy','success','Delete Data Berhasil');
		redirect('pages/MasterCase2');
	}
	/* ======= End Input Case 2 ====== */

	/* ======= Faq ==========*/
	
	public function inputFaq()
	{
		if ($this->input->post('title')) {
			
			$data_title = array();
			$data_description = array();
		    $data_image = array();
			
			$files = $_FILES;
		    for($i = 0; $i < count($_FILES['img_faq']['name']) ; $i++)
		    {           
		        $_FILES['img_faq']['name']		= 	$files['img_faq']['name'][$i];
		        $_FILES['img_faq']['type']		= 	$files['img_faq']['type'][$i];
		        $_FILES['img_faq']['tmp_name']	= 	$files['img_faq']['tmp_name'][$i];
		        $_FILES['img_faq']['error']		= 	$files['img_faq']['error'][$i];
		        $_FILES['img_faq']['size']		= 	$files['img_faq']['size'][$i];    
		       	$this->imageConf();
		       	if (!$this->upload->do_upload('img_faq')){
		       		$this->setMessages('Oooppsss','error',strip_tags($this->upload->display_errors()));
					redirect('pages/faq');
		       	}else{
		       		$gambar = $this->upload->data('file_name');
					$data_image[] = $gambar;
		       	}
		    }

			for($i=0; $i < count($this->input->post('title')); $i++){

				if($this->input->post('title')[$i] != ""){
					$title		= $this->input->post('title')[$i];
					$data_title[] = $title;
				}
			}

			for($i=0; $i < count($this->input->post('description')); $i++){

				if($this->input->post('description')[$i] != ""){
					$description		= $this->input->post('description')[$i];
					$data_description[] = $description;
				}
			}

			// print_r(json_encode($data_image)); die;
			
			$data = array(
		    	'id_content' 	=> $this->uuid(),
				'id_category'	=> $this->input->post('fk_id_category'),
				'img_content' 	=> json_encode($data_image),
				'title'      	=> json_encode($data_title),
				'description' 	=> json_encode($data_description),
				'created_by' 	=> $this->session->userdata('LOGGED_USERNAME'),
				'created_date' 	=> date('Y-m-d H:i:s'),
				'is_delete' 	=> "0",
			);
			$result = $this->model->insert('content_faq', $data);
			// var_dump($data); die;
			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/faq');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/faq');
			}

		}
	}

	public function editFaq($id)
	{
		$this->data['dataFaq'] = $this->model->readTable('content_faq')->result();
		$this->data['categoryFaq'] = $this->model->readTable('category_faq')->result();
		$this->data['joinDataFaq'] = $this->model->getTableJoinContent();
		$this->data['faqEdit']	= $this->db->select('*')->where('id_content', $id)->get('content_faq')->row();
		$this->data['resultFaq']	= $this->db->select('*')->where('id_content', $id)->get('content_faq')->result();
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Dashboard Faq Edit';
		$this->data['content'] = 'backend/content/section/faq_edit';
		$this->data['menuActiveSection'] = 'active';
		$this->data['menuOpenSection'] = 'menu-open';
		$this->data['faq'] = 'active';
		// print_r($this->data['resultFaq'][0]->id_category); die;
		$this->load->view('MainView', $this->data);
	}

	public function editFaqProcess()
	{
		$id 		 = $this->input->post('id_content',TRUE);	// get id banner//
		$id_category = $this->input->post('id_category',TRUE);	// get id banner//
	
		$data_title = array();
		$data_description = array();
	    $data_image = array();

		if (!empty($_FILES['img_content']['name'][0])) {
			$files = $_FILES;
			$get_image = $this->db->select('*')->where('id_content', $id)->from('content_faq')->get()->row();
			$labels = array();

			foreach ((object)json_decode($get_image->img_content) as $key => $value) {
				$image_path = 'assets/img/';
				$labels[] = [
					'img' => unlink($image_path.$value)
				];
			}
		    
		    for($i = 0; $i < count($_FILES['img_content']['name']); $i++)
		    {           
		        $_FILES['img_content']['name']		= 	$files['img_content']['name'][$i];
		        $_FILES['img_content']['type']		= 	$files['img_content']['type'][$i];
		        $_FILES['img_content']['tmp_name']	= 	$files['img_content']['tmp_name'][$i];
		        $_FILES['img_content']['error']		= 	$files['img_content']['error'][$i];
		        $_FILES['img_content']['size']		= 	$files['img_content']['size'][$i];    
		       	$this->imageConf();
		       	if (!$this->upload->do_upload('img_content')){
		       		$this->setMessages('Oooppsss','error',strip_tags($this->upload->display_errors()));
					redirect('pages/faq');
		       	}else{
		       		$img_faq_data = $this->upload->data('file_name');
					$data_image[] = $img_faq_data;
		       	}
		    }

		    for($i=0; $i < count($this->input->post('title')); $i++){

				if($this->input->post('title')[$i] != ""){
					$title		= $this->input->post('title')[$i];
					$data_title[] = $title;
				}
			}

			for($i=0; $i < count($this->input->post('description')); $i++){

				if($this->input->post('description')[$i] != ""){
					$description		= $this->input->post('description')[$i];
					$data_description[] = $description;
				}
			}
			
			$data = array(
				'id_category' 	  => $id_category, 
				'img_content' 	  => json_encode($data_image), 
				'title' 	  	  => json_encode($data_title), 
				'description' 	  => json_encode($data_description), 
			);

			$this->db->where('id_content', $id);
			$result = $this->db->update('content_faq', $data);

			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/faq');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/faq');
			}

		}else{
			for($i=0; $i < count($this->input->post('title')); $i++){

				if($this->input->post('title')[$i] != ""){
					$title		= $this->input->post('title')[$i];
					$data_title[] = $title;
				}
			}

			for($i=0; $i < count($this->input->post('description')); $i++){

				if($this->input->post('description')[$i] != ""){
					$description		= $this->input->post('description')[$i];
					$data_description[] = $description;
				}
			}
			
			$data = array(
				'id_category' 	  => $id_category, 
				'title' 	  	  => json_encode($data_title), 
				'description' 	  => json_encode($data_description), 
			);

			$this->db->where('id_content', $id);
			$result = $this->db->update('content_faq', $data);

			if ($result === true) {
				$this->setMessages('Yeeaayy','success','Menambahkan Data Berhasil');
				redirect('pages/faq');
			}else{
				$this->setMessages('Oooppsss','error','Menambahkan Data Gagal');
				redirect('pages/faq');
			}
		}


	}

	/* ======= End Faq ==========*/

}
