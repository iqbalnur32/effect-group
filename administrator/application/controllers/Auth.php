<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	function __construct() {
		parent::__construct();
		if ($this->session->userdata('LOGGED_TOKEN')) {
			redirect('/');
		}
	}

	public function setMessage($title = null, $type = null, $message = null) {
		return $this->session->set_flashdata([
			'MESSAGE_type' => $type,
			'MESSAGE_title' => $title,
			'MESSAGE_message' => $message
		]);
	}

	public function index() {
		if ($this->input->post()) {
			$this->load->model('MasterModel','master',TRUE);
			$admin = $this->master->getAdminByUsername($this->input->post('username'));
			if ($admin) {
				if (password_verify($this->input->post('password'),$admin->password)) {
					$this->session->set_userdata([
						'LOGGED_ID' => $admin->id_user,
						'LOGGED_USERNAME' => $admin->username,
						'LOGGED_FULLNAME' => $admin->full_name,
						'LOGGED_ROLE' => $admin->id_role,
						'LOGGED_TOKEN' => sha1(crypt(date('sis').$admin->password,''))
					]);
					$this->setMessage('Yeeaayy','success','Login berhasil, selamat datang kembali '.$admin->full_name.'!');
					redirect('/');
				} else {
					$this->setMessage('Oooppsss','error','Username dan password tidak sesuai');
					redirect('Auth');
				}
			} else {
				$this->setMessage('Oooppsss','error','Username tidak terdaftar');
				redirect('Auth');
			}
		} else {
			$this->load->view('backend/auth');
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */