<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Handler extends CI_Controller {

	public function index() {
		redirect('MainPage');
	}
	public function not_found() {
		$this->load->view('errors/html/404_custom');
	}
}

/* End of file Handler.php */
/* Location: ./application/controllers/Handler.php */