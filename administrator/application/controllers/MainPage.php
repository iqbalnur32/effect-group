<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainPage extends MY_Controller {

	public function index(){
		$this->data['totalcontent'] = $this->model->readTable('content_faq')->num_rows();
		$this->data['title'] = 'Dashboard - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Dashboard';
		$this->data['content'] = 'backend/content/dashboard';
		$this->data['dashboardActive'] = 'active';
		$this->load->view('MainView',$this->data);
	}
	
	public function inputsectionhome(){
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Input Section Home';
		$this->data['content'] = 'backend/content/input/section_home.php';
		$this->data['menuActive'] = 'active';
		$this->data['menuOpenInput'] = 'menu-open';
		$this->data['inputSectionHome'] = 'active';
		$this->load->view('MainView',$this->data);
	}


	public function inputAboutHome(){
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Input About Home';
		$this->data['content'] = 'backend/content/input/about_home.php';
		$this->data['menuActive'] = 'active';
		$this->data['menuOpenInput'] = 'menu-open';
		$this->data['inputAboutHome'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function inputbanner(){
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Input Banner';
		$this->data['content'] = 'backend/content/input/banner';
		$this->data['menuActive'] = 'active';
		$this->data['menuOpenInput'] = 'menu-open';
		$this->data['inputHomeBanner'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function inputUseCase1()
	{
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Input Case 1';
		$this->data['content'] = 'backend/content/input/use_case_1.php';
		$this->data['menuActive'] = 'active';
		$this->data['menuOpenInput'] = 'menu-open';
		$this->data['inputCase1'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function inputUseCase2()
	{
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Input Case 2';
		$this->data['content'] = 'backend/content/input/use_case_2.php';
		$this->data['menuActive'] = 'active';
		$this->data['menuOpenInput'] = 'menu-open';
		$this->data['inputCase2'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function inputcontent(){
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Input Content';
		$this->data['content'] = 'backend/content/input/content';
		$this->data['menuActive'] = 'active';
		$this->data['menuOpenInput'] = 'menu-open';
		$this->data['inputXIIActive'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function faq(){
		$this->data['dataFaq'] = $this->model->readTable('content_faq')->result();
		$this->data['categoryFaq'] = $this->model->readTable('category_faq')->result();
		$this->data['joinDataFaq'] = $this->model->getTableJoinContent();
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Faq';
		$this->data['content'] = 'backend/content/section/faq';
		$this->data['menuActiveSection'] = 'active';
		$this->data['menuOpenSection'] = 'menu-open';
		$this->data['faq'] = 'active';
		$this->load->view('MainView',$this->data);
	}
	
	public function inputNavigation(){
		$this->data['dataNavigation'] = $this->model->readTable('navigation')->result();
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Navigation Header';
		$this->data['content'] = 'backend/content/section/v_navigation';
		$this->data['menuActiveSection'] = 'active';
		$this->data['menuOpenSection'] = 'menu-open';
		$this->data['inputFooter'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function sectionEffectGroup()
	{
		$this->data['dataNavigation'] = $this->model->readTable('navigation')->result();
		$this->data['dataSection1'] = $this->model->readTable('section_effect_1')->result();
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Section Effect Group';
		$this->data['content'] = 'backend/content/section/v_section_1';
		$this->data['menuActiveSection'] = 'active';
		$this->data['menuOpenSection'] = 'menu-open';
		$this->data['inputSection1'] = 'active';
		$this->load->view('MainView',$this->data);
	}
	
	public function sectionEffectGroup2()
	{
		$this->data['dataNavigation'] = $this->model->readTable('navigation')->result();
		$this->data['dataSection2'] = $this->model->readTable('section_effect_2')->result();
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['contentTitle'] = 'Section Effect Group 2';
		$this->data['content'] = 'backend/content/section/v_section_2';
		$this->data['menuActiveSection'] = 'active';
		$this->data['menuOpenSection'] = 'menu-open';
		$this->data['inputSection2'] = 'active';
		$this->load->view('MainView',$this->data);
	}
	
	public function MasterSectionHome(){
		$this->data['title'] = 'Input - '.$this->mainTitle;
		$this->data['dataSectionHome'] = $this->model->readTable('section_home')->result();
		$this->data['contentTitle'] = 'Dara Discount';
		$this->data['content'] = 'backend/content/data/section_home.php';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['editDiscountActive'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function MasterLogo(){
		$this->data['title'] = 'Data - '.$this->mainTitle;
		$this->data['dataBannerDB'] = $this->model->readTable('banner')->result();
		$this->data['contentTitle'] = 'Data About Home';
		$this->data['content'] = 'backend/content/data/about_home';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['dataLogo'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function MasterCase1(){
		$this->data['title'] = 'Data - '.$this->mainTitle;
		$this->data['dataBannerDB'] = $this->model->readTable('banner')->result();
		$this->data['dataCase1'] = $this->model->readTable('use_case_1')->result();
		$this->data['dataCase2'] = $this->model->readTable('use_case_2')->result();
		$this->data['contentTitle'] = 'Data Master 1';
		$this->data['content'] = 'backend/content/data/data_case_1';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['dataLogo'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function MasterCase2(){
		$this->data['title'] = 'Data - '.$this->mainTitle;
		$this->data['dataBannerDB'] = $this->model->readTable('banner')->result();
		$this->data['dataCase1'] = $this->model->readTable('use_case_1')->result();
		$this->data['dataCase2'] = $this->model->readTable('use_case_2')->result();
		$this->data['contentTitle'] = 'Data Master 2';
		$this->data['content'] = 'backend/content/data/data_case_2';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['dataMenuCase2'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function MasterAboutHome(){
		$this->data['title'] = 'Data - '.$this->mainTitle;
		$this->data['dataBannerDB'] = $this->model->readTable('banner')->result();
		$this->data['dataAboutUs'] = $this->model->readTable('about_us_home')->result();
		$this->data['contentTitle'] = 'About Home Section';
		$this->data['content'] = 'backend/content/data/about_home';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['dataXActive'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function MasterBanner(){
		$this->data['title'] = 'Data - '.$this->mainTitle;
		$this->data['dataBannerDB'] = $this->model->readTable('banner')->result();
		$this->data['contentTitle'] = 'Data Banner';
		$this->data['content'] = 'backend/content/data/banner';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['dataBanner'] = 'active';
		$this->load->view('MainView',$this->data);
	}

	public function MasterContent(){
		$this->data['title'] = 'Data - '.$this->mainTitle;
		$this->data['listContent'] = $this->model->readTable('content')->result();
		$this->data['dataContentJoin'] = $this->model->getTableJoinContent();
		$this->data['contentTitle'] = 'Data Content';
		$this->data['content'] = 'backend/content/data/content';
		$this->data['menuActiveData'] = 'active';
		$this->data['menuOpenData'] = 'menu-open';
		$this->data['dataContent'] = 'active';
		$data = $this->data['dataContentJoin'];
		$this->load->view('MainView',$this->data);
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('Auth');
	}
}