<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterModel extends CI_Model{
  
  public function readTable($table){
    return $this->db->get($table);
  }

  public function readTableContent($select, $table)
  {
    return $this->db->select($select)->get($table);
  }
  
  public function readTableContentOrder($select, $column , $type ,$table)
  {
    return $this->db->select($select)->order_by($column, $type)->get($table);
  }
  
  public function insert($table,$data){
    return $this->db->insert($table,$data);
  }
  
  public function update($table,$data,$id){
    $this->db->where('id_category ', $id);
    return $this->db->update($table,$data);
    // return $this->db->update($table,$data,$where);
  }

  /* Model Delete */
  public function delete($table, $id_column, $id_key)
  {
    // $this->db->where('pemohon.id_pemohon=user.id_user');
     $this->db->delete($table, array($id_column => $id_key));
  }

  public function getAdminByUsername($admin_username,$id_admin = null) {
		if ($id_admin) {
			$this->db->where('id_user', $id_admin);
		}
		$this->db->where('username', $admin_username);
		// $this->db->where('admin_hide', 0);
		return $this->db->get('user')->row_object();
	}

  /* Model Get All Data */
  public function getTable($table)
  {
    return $this->db->from($table)->get()->result();
  }

  /*Model Join Content*/
  public function getTableJoinContent()
  {
    return $this->db->from('content_faq')->join('category_faq', 'category_faq.id_category = content_faq.id_category', 'inner')->get()->result();
  }

  /* Model Get Edit Table */
  public function getEditTable($table, $id_column, $id_key)
  {
    if ($id_key) {
      return $this->db->from($table)
                ->where($id_column, $id_key)
                ->get()
                ->row();
    }
  }
  // public function totalSiswaX() {
  //   return $this->db->query("SELECT COUNT(*) AS count FROM tb_rekap_x")->row_object();
  // }

  // public function totalSiswaXI() {
  //   return $this->db->query("SELECT COUNT(*) AS count FROM tb_rekap_xi")->row_object();
  // }

  // public function totalSiswaXII() {
  //   return $this->db->query("SELECT COUNT(*) AS count FROM tb_rekap_xii")->row_object();
  // }

  // public function totalSeluruhSiswa() {
  //  return  $this->db->query("SELECT (x.num + xi.num + xii.num) AS count FROM
  //                   (SELECT COUNT(*) as num FROM tb_rekap_x) as x,
  //                   (SELECT COUNT(*) as num FROM tb_rekap_xi) as xi,
  //                   (SELECT COUNT(*) as num FROM tb_rekap_xii) as xii")->row_object();
  // }



}
