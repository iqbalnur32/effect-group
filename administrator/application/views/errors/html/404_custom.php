<!DOCTYPE html>
<html lang="en" class="pos-relative">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="3S Aqiqah ( Syari, Simple, Sedap )">
	<meta name="author" content="Aqiqah86 Developer">
	<title>Halaman tidak ditemukan</title>
	<link href="<?= base_url('assets/lib/@fortawesome/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/css/bracket.css') ?>">
</head>

<body class="pos-relative">

	<div class="ht-100v d-flex align-items-center justify-content-center">
		<div class="wd-lg-70p wd-xl-50p tx-center pd-x-40">
			<h1 class="tx-100 tx-xs-140 tx-normal tx-inverse tx-roboto mg-b-0">404!</h1>
			<h5 class="tx-xs-24 tx-normal tx-info mg-b-30 lh-5">Ooopsss halaman tidak tersedia</h5>
			<p class="tx-16 mg-b-30">
				Halaman yang anda cari kemungkinan telah dihapus, ada perubahan nama, atau tidak tersedia, silahkan hubungi pihak admin untuk informasi lebih lanjut!
			</p>
			<div class="d-flex justify-content-center">
				<a href="<?= site_url() ?>" class="btn btn-primary"><i class="fa fa-home"></i> Halaman Utama</a>
			</div><!-- d-flex -->
		</div>
	</div><!-- ht-100v -->

	<script src="<?= base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/lib/jquery-ui/ui/widgets/datepicker.js') ?>"></script>
	<script src="<?= base_url('assets/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

</body>
</html>
