<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?= $title ?></title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta http-equiv="x-ua-compatible" content="ie=edge">
  	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url('assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/adminlte/plugins/fontawesome-free/css/all.min.css') ?>">
  	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/sweetalert/sweetalert.css') ?>">
	  
  	<!-- overlayScrollbars -->
  	<link rel="stylesheet" href="<?= base_url('assets/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') ?>">
  	<!-- Theme style -->
 	 <link rel="stylesheet" href="<?= base_url('assets/adminlte/dist/css/adminlte.min.css')?>">
  	<!-- Google Font: Source Sans Pro -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body class="sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed" style="height: auto;">
	<div class="wrapper">

		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<?php $this->load->view($navbar); ?>
		</nav>

		<aside class="main-sidebar sidebar-light-primary elevation-4">
			<?php $this->load->view($sidebar); ?>
		</aside>

		<div class="content-wrapper" style="min-height: 120px;">
			<div class="content-header">
    		<div class="container-fluid">
      		<div class="row mb-2">
      			<div class="col-sm-6">
      				<h1><?= $contentTitle ?></h1>
      			</div>
        	</div>
        </div>
		<!-- content -->
	      <section class="content">
		     	<div class="container-fluid">
		     		<?php $this->load->view($content); ?>
		     	</div>
	     	</section>
	    <!-- end content -->
	    </div>

		<footer class="main-footer">
			<strong>Copyright © 2020 <a href="https://alwan-puta.dev">{ We-Dev }</a>.</strong>All rights reserved.
		    <div class="float-right d-none d-sm-inline-block">
		      <b>Version</b> 3.0.0
		    </div>
	 	</footer>
	</div>

	<!-- jQuery -->
	<script src="<?= base_url('assets/adminlte/plugins/jquery/jquery.min.js') ?>"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url('assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
	<!-- overlayScrollbars -->
	<script src="<?= base_url('assets/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url('assets/adminlte/dist/js/adminlte.js')?>"></script>

	<!-- OPTIONAL SCRIPTS -->
	<script src="<?= base_url('assets/adminlte/dist/js/demo.js')?>"></script>

	<!-- PAGE PLUGINS -->
	<!-- jQuery Mapael -->
	<script src="<?= base_url('assets/adminlte/plugins/datatables/jquery.dataTables.js')?>"></script>
	<script src="<?= base_url('assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
	<script src="<?= base_url('assets/adminlte/plugins/jquery-mousewheel/jquery.mousewheel.js')?>"></script>
	<script src="<?= base_url('assets/adminlte/plugins/raphael/raphael.min.js') ?>"></script>
	<script src="<?= base_url('assets/adminlte/plugins/jquery-mapael/jquery.mapael.min.js') ?>"></script>
	<script src="<?= base_url('assets/adminlte/plugins/jquery-mapael/maps/usa_states.min.js') ?>"></script>
	<!-- ChartJS -->
	<script src="<?= base_url('assets/adminlte/plugins/chart.js/Chart.min.js')?>"></script>
	<script src="<?= base_url('assets/adminlte/dist/js/chartjs-plugin-datalabels.min.js')?>"></script>

	<!-- PAGE SCRIPTS -->
	<script src="<?= base_url('assets/adminlte/dist/js/pages/dashboard2.js')?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/sweetalert/sweetalert.min.js') ?>"></script>
	<!-- panggil ckeditor.js -->	
	<script type="text/javascript" src="<?= base_url('assets/ckeditor/ckeditor.js') ?>"></script>
	<!-- Javascript Custom Cok -->
	<!-- <script type="text/javascript" src="<?= base_url('assets/view_js/master.js') ?>"></script> -->
	 <!-- panggil ckeditor.js -->
  	<script type="text/javascript" src="<?= base_url('assets/ckeditor/ckeditor.js') ?>"></script>
  	<!-- panggil adapter jquery ckeditor -->
  	<script type="text/javascript" src="<?= base_url('assets/ckeditor/adapters/jquery.js') ?>"></script>
  	<!-- setup selector -->
	
	<script>
	  $(function () {
	    // $("#example1").DataTable();
	    $('.table').DataTable({
	      "paging": true,
	      "lengthChange": true,
	      "searching": true,	
	      "ordering": true,
	      "info": true,
	      "autoWidth": true
	    });
	    // CK Editor
	  	// CKEDITOR.replace( 'description' );
	  	$('#description').ckeditor();
	  	$('.text-description').ckeditor();
	  });
	</script>
	<script>
		$(function () {
			var title = '<?= $this->session->userdata('MESSAGE_title') ?>';
			var type = '<?= $this->session->userdata('MESSAGE_type') ?>';
			var message = '<?= $this->session->userdata('MESSAGE_message') ?>';
			if(title && type && message) {
				swal({
					title: title,
					type: type,
					text: message,
					timer: 10000,
					showConfirmButton: true
				});
			}
		});
	</script>

	<script type="text/javascript">
		var base_url = '<?= base_url() ?>';

		/* ==== Modal Edit Naviagtion ====== */
		$(document).on("click", "#modal_edit_navigation", function() {
			var id_nav = $(this).data('id_nav');
			var title_navigation = $(this).data('title_navigation');
			var link_navigation = $(this).data('link_navigation');
			
			$('#id_nav').val(id_nav)
			$('#title_navigation').val(title_navigation)
			$('#link_navigation').val(link_navigation)
		})
		/* ==== End Modal Edit Naviagtion ====== */

	    /* Modal Edit Category */
	    $(document).on('click', '#modal_category', function() {

			var id_category = $(this).data('id_category');	
			// console.log(id_category);
			$.ajax({
				url: base_url + 'action/Master/editHomePage/' + id_category,
				type: 'GET',	
				dataType: 'JSON',	
				success: function(data){
					$('#category').val(data.name_category);
					$('#id_category').val(data.id_category);
				},
				error: function(err){
					// console.log(err);
					swal({title: 'Oooppsss', type: 'error',text: err,timer: 10000, showConfirmButton: true});
				}
			})
		})
        
        /* Modal Edit Section Home */
		$(document).on('click', '#edit_section_home', function() {
			var id_home = $(this).data('id_home');
			var title_home = $(this).data('title_home');
			var description_home = $(this).data('description_home');
			var icons_home = $(this).data('icons_home');
			var image_home = $(this).data('image_home');
			$('#id_home').val(id_home)
			$('#title_home').val(title_home)
			$('#description_home').val(description_home)
			$('#icons_home').val(icons_home)
			$('#image_home').val(image_home)
		})
    
		/*  ======  Data Master Banner =====  */
		$(document).on('click', '#banner_category', function() {
			var id_banner = $(this).data('id_banner');
			$.ajax({
				url: base_url + 'action/Master/editBanner/' + id_banner,
				type: 'GET',	
				dataType: 'JSON',
				success: function(data){
					$('#id_banner').val(id_banner)
					$('#nama_banner').val(data.nama_banner)
					$('#description_banner').val(data.description)
				},
				error: function(err){
					console.log(err);
				}
			}) 
		})

		/* ====== Edit Section 2 ======*/
		$(document).on("click", "#modal_edit_section_2", function() {
			var id_section_2 = $(this).attr('data-id_section_2')
			var title = $(this).attr('data-title')
			var description = $(this).attr('data-description')
			var img_content = $(this).attr('data-img_content')
			const parse = JSON.parse(img_content)

			$('#id_section_2').val(id_section_2)
			$('#title').val(title)
			$('#description').val(description)
			parse.map((key, index) => {
				$('#img_section2').attr("src", base_url + "assets/img/"+key); 
			})
		})
		/* ====== End Edit Section 2 ======*/

		/* ======= Master Use Case 1 ======*/
		$(document).on("click", "#modal_use_case_1", function() {
			var id_use_case_1 = $(this).attr('data-id_use_case_1')
			var title = $(this).attr('data-title')
			var description = $(this).attr('data-description')

			$('#id_use_case_1').val(id_use_case_1)
			$('#title').val(title)
			$('#description').val(description)
		})
		/* ======= End Master Use Case 1 ======*/

		/* ======= Master Use Case 2 ======*/
		$(document).on("click", "#modal_use_case_2", function() {
			var id_use_case_2 = $(this).attr('data-id_use_case_2')
			var title = $(this).attr('data-title')
			var description = $(this).attr('data-description')

			$('#id_use_case_2').val(id_use_case_2)
			$('#title').val(title)
			$('#description').val(description)
		})
		/* ======= End Master Use Case 2 ======*/
	
	</script>
    </body>
    </html>
