 <a href="" class="brand-link">
    <!-- <img src="<?= base_url('assets/img/bakdat.png')?>" alt="Bakdat Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
    <span class="brand-text font-weight-light"><b>DASHBOARD ADMIN</b></span>
  </a>

  <div class="sidebar">
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item has-treeview ">
          <a href="<?= site_url()?>" class="nav-link <?= $dashboardActive ?>">
            <i class="nav-icon fas fa-home"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item has-treeview ">
          <a href="<?= base_url('pages/orderhistory')?>" class="nav-link <?= $orderHistoryActive ?>">
            <i class="fa fa-shopping-cart"></i>
            <p>Confirm Order</p>
          </a>
        </li>
        <li class="nav-item has-treeview <?= $menuOpenInput ?>">
          <a href="#" class="nav-link <?= $menuActive ?>">
            <i class="nav-icon fas fa-edit"></i>
            <p>
              Input Master Data
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview <?= $menuActive ?>">
            <li class="nav-item">
              <a href="<?= base_url('pages/inputbanner') ?>" class="nav-link <?= $inputHomeBanner ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Home Banner</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/inputsectionhome')?>" class="nav-link <?= $inputSectionHome ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Input Section Home</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/inputAboutHome')?>" class="nav-link <?= $inputAboutHome ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Input About Home</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/inputUseCase1')?>" class="nav-link <?= $inputCase1 ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Input Use Case 1</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/inputUseCase2')?>" class="nav-link <?= $inputCase2 ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Input Use Case 2</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/inputcontent')?>" class="nav-link <?= $inputXIIActive ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Content</p>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="<?= base_url('pages/inputcontent')?>" class="nav-link <?= $inputXIIActive ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Instagram Feed</p>
              </a>
            </li> -->
          </ul>
        </li>
<!---------------------->
        <li class="nav-item has-treeview <?= $menuOpenData ?>">
          <a href="#" class="nav-link <?= $menuActiveData ?>">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Master Data
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview <?= $menuActiveData ?> ">
            <li class="nav-item">
              <a href="<?= base_url('pages/MasterBanner') ?>" class="nav-link <?= $dataBanner ?> ">
                <i class="far fa-circle nav-icon"></i>
                <p>Home Banner</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/MasterSectionHome')?>" class="nav-link <?= $editDiscountActive ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Section Home</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/MasterAboutHome')?>" class="nav-link <?= $dataXActive ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>About Home</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/MasterCase1')?>" class="nav-link <?= $dataLogo ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Use Case 1</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/MasterCase2') ?>" class="nav-link <?= $dataMenuCase2 ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Use Case 2</p>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="<?= base_url('') ?>" class="nav-link <?= $dataContent ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Instagram Fedd</p>
              </a>
            </li> -->
          </ul>
        </li>
        <!---------------------->
        <li class="nav-item has-treeview <?= $menuOpenSection ?>">
          <a href="#" class="nav-link <?= $menuActiveSection ?>">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Faq & Effect Group & Princing
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview <?= $menuActiveSection ?> ">
            <li class="nav-item">
              <a href="<?= base_url('pages/faq')?>" class="nav-link <?= $faq ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Faq</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/sectionEffectGroup')?>" class="nav-link <?= $inputSection1 ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Section Effect Group 1</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/sectionEffectGroup2')?>" class="nav-link <?= $inputSection2 ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Section Effect Group 2</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url('pages/inputNavigation')?>" class="nav-link <?= $inputFooter ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Navigation</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- <li class="nav-item has-treeview <?= $menuOpenGrafik ?>">
          <a href="#" class="nav-link <?= $menuActiveGrafik ?>">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
              Trafic Visitor
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?= base_url('pages/grafikSiswaX') ?>" class="nav-link <?= $grafikXActive ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Grafik-X</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="grafik-xi.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Grafik-XI</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="grafik-xii.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Gratik-XII</p>
              </a>
            </li>
          </ul>
        </li> -->
        <li class="nav-item">
          <a href="<?= base_url('pages/logout') ?>" class="nav-link">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
              Logout
            </p>
          </a>
        </li>

          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
