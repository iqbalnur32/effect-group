<?php error_reporting(0) ?>
<div class="card card-primary">
	<form method="POST" action="<?= base_url('action/Master/editFaqProcess')?>" enctype="multipart/form-data">
	  	<div class="card-body">
		    <div class="row">
		    	<div class="col-md-12">
					<div class="form-group">
						<label>Category</label>
                        <input type="hidden" class="form-control border-black" name="id_content" value="<?php echo $faqEdit->id_content ?>" >
						<select class="form-control" name="id_category">
							<?php foreach ($categoryFaq as $key => $value): ?>
								<option value="<?= $value->id_category ?>" <?= $value->id_category == $faqEdit->id_category ? "selected" : "" ?> ><?= $value->name_category ?></option>
							<?php endforeach ?>
						</select>
					</div>	    	
		    	</div>
		    	<div class="col-md-12">
		    		 <label>Faq Content</label>
		    		<div class="table-responsive">
	                  <table class="table table-bordered mb-0">
	                    <thead>
	                      <tr>
	                        <th style="width: 2px;">No</th>
	                        <th>Image Result</th>
	                        <th>Image</th>
	                        <th>Title</th>
	                        <th>Description</th>
	                      </tr>
	                    </thead>
	                    <tbody>
                          <?php $title_result = json_decode($faqEdit->title); ?>
                          <?php $description_result = json_decode($faqEdit->description); ?>
                          <?php $image_result = json_decode($faqEdit->img_content); ?>
	                      <?php for($i = 0; $i <= 2 ; $i++) { ?>
	                        <tr>
	                          <td><?php echo $i ?></td>	
	                          <td><img src=" <?= base_url().'assets/img/'.$image_result[$i] ? base_url().'assets/img/'.$image_result[$i] : "https://www.freeiconspng.com/uploads/no-image-icon-15.png" ?>" style="width: 100px; height:100px;"></td>
	                          <td><input class="form-control" type="file" name="img_content[]"></td>
	                          <td><input class="form-control" type="text" name="title[]" placeholder="Required Title ...." value="<?= $title_result[$i] ?>"></td>
	                          <td><input class="form-control" type="text" name="description[]" placeholder="Required Description ...." value="<?= $description_result[$i] ?>"></td>
	                        </tr>
	                      <?php }?>
	                    </tbody>
	                  </table>
	              	</div>
		    	</div>
		    </div>
		    <br>
		    <div class="row">
		    	<div class="col-md-12">
		    		<div class="float-left">
		    			<button class="btn btn-xl btn-primary" type="submit">Update</button>
		    		</div>
		    	</div>
		    </div>
		</div>
	</form>
</div>