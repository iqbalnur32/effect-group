<div class="card card-primary">
  <div class="card-body">
     <!-- Button trigger modal -->
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSubcriber">
            Tambah
            </button>
        </div>
    </div>
    <br>
     <table class="table table-bordered">
 		<thead>
 			<tr>
 				<th>No</th>
 				<th>Nama Subcriber</th>
 				<th>Image Subcriber</th>
 				<th>Action</th>
 			</tr>
 		</thead>
 		<tbody>
 		<?php $i = 1; foreach ($dataSubcriber as $key): ?>
 			<tr>
 				<td><?= $i++ ?></td>
 				<td><?= $key->title_sucriber ?></td>
 				<td><img style="height:50px; width:50px" src="<?= base_url('assets/img/').$key->img_subcriber ?>"></td>
 				<td>
 					<a id="modal_subcriber" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalEditSubcriber"
 					  data-id_subcriber="<?= $key->id_sucriber ?>"
					  data-title_sucriber="<?= $key->title_sucriber ?>"
					  data-img_subcriber="<?= $key->img_subcriber ?>"
 					><i class="fas fa-pencil-alt"></i></a>				
 					<a href="<?= base_url('action/Master/deleteSubcriberProcess/').$key->id_sucriber ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
 				</td>
 			</tr>
 		<?php endforeach ?>
 		</tbody>
 	</table>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalSubcriber" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/inputSubcribtion')?>" method="post" enctype="multipart/form-data" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Subcription Add</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Title Subcriber</label>
                      <input class="form-control" name="title_subcriber" placeholder="Masukan Judul Subcriber" required />
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Image Subcriber</label>
                      <input type="file" class="form-control" name="img_subcriber" required id="customFile">
                  </div>
              </div>
              <small style="color: red">* Pastikan ukuran gambar harus 770x463 untuk hasil gambar yang bagus</small>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEditSubcriber" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/editSubcriberProcess')?>" method="post" enctype="multipart/form-data" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Section</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Title Subcriber</label>
                      <input class="form-control" type="text" name="title_sucriber" placeholder="Masukan Judul Subcriber" id="title_sucriber" required />
                      <input class="form-control" type="hidden" name="id_sucriber" placeholder="Masukan Judul Subcriber" id="id_subcriber" required />
                  </div>
              </div>
              <div class="form-group">
                <img style="width: 100px; height: 100px;" src="" id="img">
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Image Subcriber</label>
                      <input type="file" class="form-control" name="img_subcriber" id="customFile">
                  </div>
              </div>
               <small style="color: red">* Pastikan ukuran gambar harus 1920x800 untuk hasil gambar yang bagus</small>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
