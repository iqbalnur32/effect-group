<div class="card card-primary">
  <div class="card-body">
     <!-- Button trigger modal -->
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSection">
            Tambah
            </button>
        </div>
    </div>
    <br>
     <table class="table table-bordered">
 		<thead>
 			<tr>
 				<th>No</th>
 				<th>Nama Section</th>
 				<th>Action</th>
 			</tr>
 		</thead>
 		<!-- <tbody>
 		<?php $i = 1; foreach ($dataSection as $key): ?>
 			<tr>
 				<td><?= $i++ ?></td>
 				<td><?= $key->title_section ?></td>
 				<td>
 					<a id="modal_section" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalEditSection"
 					  data-id_category="<?= $key->id_section ?>"
					  data-name_category="<?= $key->title_section ?>"
					  data-fk_id_category="<?= $key->fk_id_category ?>"
 					><i class="fas fa-pencil-alt"></i></a>				
 					<a href="<?= base_url('action/Master/deleteSectionProcess/').$key->id_section ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
 				</td>
 			</tr>
 		<?php endforeach ?>
 		</tbody> -->
 	</table>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalSection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/inputSection')?>" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Section Add</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Judul Section</label>
                      <input class="form-control" name="title_section" placeholder="Masukan Judul Section" required />
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Category Section</label>
                      <select class="form-control" name="fk_id_category">
                           <?php foreach ($listcategory as $data) { ?>
                              <option value="<?= $data->id_category ?>"><?= $data->name_category ?></option>
                          <?php } ?>
                      </select>
                  </div>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>