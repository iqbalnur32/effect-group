<div class="card card-primary">
  <div class="card-body">
     <!-- Button trigger modal -->
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFooter">
            Tambah
            </button>
        </div>
    </div>
    <br>
     <table class="table table-bordered">
 		<thead>
 			<tr>
 				<th>No</th>
 				<th>Nama Subcriber</th>
 				<th>Link Navigation</th>
 				<th>Action</th>
 			</tr>
 		</thead>
 		<tbody>
 		<?php $i = 1; foreach ($dataNavigation as $key): ?>
 			<tr>
 				<td><?= $i++ ?></td>
 				<td><?= $key->title_navigation ?></td>
 				<td><?= $key->link_navigation  ?></td>
 				<td>
 					<a id="modal_edit_navigation" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalNavigation"
 					  data-id_nav="<?= $key->id_nav ?>"
					  data-title_navigation="<?= $key->title_navigation ?>"
					  data-link_navigation="<?= $key->link_navigation ?>"
 					><i class="fas fa-pencil-alt"></i></a>				
 					<a href="<?= base_url('action/Master/deleteNavigationProcess/').$key->id_nav ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
 				</td>
 			</tr>
 		<?php endforeach ?>
 		</tbody>
 	</table>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalFooter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/inputNavigationProcess')?>" method="post"  enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Footer Add</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Image Navigation</label>
                  <input class="form-control" type="file" name="img_navigation">
                </div>
              </div>
              <div class="table-responsive">
                  <table class="table table-bordered mb-0">
                    <thead>
                      <tr>
                        <th style="width: 2px;">No</th>
                        <th>Title Navigation</th>
                        <th>Link Navigation</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i = 1; $i <= 15 ; $i++) { ?>
                        <tr>
                          <td><?php echo $i ?></td>
                          <td><input class="form-control" type="text" name="title_navigation[]" placeholder="Required Title ...."></td>
                          <td><input class="form-control" type="text" name="link_navigation[]" placeholder="Required Description ...."></td>
                        </tr>
                      <?php }?>
                    </tbody>
                  </table>
              </div>
              <!--<small style="color: red">* Pastikan ukuran gambar harus 770x463 untuk hasil gambar yang bagus</small>-->
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalNavigation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/editNavigationProcess')?>" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Navigation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Title Navigation</label>
                      <input class="form-control" type="hidden" name="id_nav" id="id_nav" required />
                      <input class="form-control" type="text" name="title_navigation" id="title_navigation" placeholder="Masukan Judul Subcriber" required />
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Link Naviigation</label>
                      <input type="text" class="form-control" name="link_navigation" id="link_navigation" placeholder="Masukan Link Subcriber" required />
                  </div>
              </div>
               <small style="color: red">* Pastikan ukuran gambar harus 1920x800 untuk hasil gambar yang bagus</small>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
