

<div class="card card-primary">
  <div class="card-body">
     <!-- Button trigger modal -->
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSection">
            Tambah
            </button>
        </div>
    </div>
    <br>
     <table class="table table-bordered">
 		<thead>
 			<tr>
 				<th>No</th>
 				<th>Nama Section</th>
 				<th>Action</th>
 			</tr>
 		</thead>
 		<tbody>
 		<?php $i = 1; foreach ($dataSection2 as $key): ?>
 			<tr>
 				<td><?= $i++ ?></td>
 				<td><?= $key->title ?></td>
 				<td>
 					<a id="modal_edit_section_2" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modaEditSection2"
            data-id_section_2="<?= $key->id_section_2 ?>"
            data-title="<?= $key->title ?>"
            data-description="<?= $key->description ?>"
            data-img_content='<?= $key->img_content ?>'><i class="fas fa-pencil-alt"></i></a>				
 					<a href="<?= base_url('action/Master/deleteSectionProcess2/').$key->id_section_2 ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
 				</td>
 			</tr>
 		<?php endforeach ?>
 		</tbody>
 	</table>
  </div>
</div>


<!-- Modal Add -->
<div class="modal fade" id="modalSection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/inputSection2')?>" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Section 2 Add</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Image Section2</label>
                <input class="form-control" type="file" name="img_section2[]">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name="title" placeholder="Required Title ....">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control text-description" type="file" name="description" placeholder="Required Description ...."></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Moda Edit -->
<div class="modal fade" id="modaEditSection2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/editSectionProcess2')?>" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Section 2 Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Image Section2</label><br>
              <img src="" id="img_section2" style="height: 100px; width: 200px;">
              <br><br>
              <input class="form-control" type="file" name="img_section2[]">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Title</label>
              <input class="form-control" type="hidden" name="id_section_2" id="id_section_2">
              <input class="form-control" type="text" name="title" placeholder="Required Title ...." id="title">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control text-description" type="file" name="description" placeholder="Required Description ...." id="description"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>