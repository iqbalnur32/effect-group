<div class="card card-primary">
  <div class="card-body">
     <!-- Button trigger modal -->
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFaq">
            Tambah
            </button>
        </div>
    </div>
    <br>
     <table class="table table-bordered">
 		<thead>
 			<tr>
 				<th>No</th>
 				<th>Title</th>
        <th>Description</th>
 				<th>Action</th>
 			</tr>
 		</thead>
 		<tbody>
 		<?php $i = 1; foreach ($joinDataFaq as $key): ?>
 			<tr>
 				<td><?= $i++ ?></td>
 				<td><?= $key->title ?></td>
        <td><?= substr($key->description, 0, 20) . '...' ?></td>
 				<td>
 					<!-- <a id="modal_content_faq" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalEditSection"
 					  data-id_content="<?= $key->id_content ?>"
					  data-id_category="<?= $key->id_category ?>"
					  data-title="<?= $key->title ?>"
            data-description="<?= $key->description ?>"
            data-img_content="<?= $key->img_content ?>"
 					><i class="fas fa-pencil-alt"></i></a>			 -->	
 					<a href="<?= base_url('action/Master/editFaq/').$key->id_content ?>" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></a>				
          <a href="<?= base_url('action/Master/deleteFaqContent/').$key->id_content ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>        
 				</td>
 			</tr>
 		<?php endforeach ?>
 		</tbody>
 	</table>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalFaq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/inputFaq')?>" method="post" enctype="multipart/form-data"> 
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Faq Content</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Category Content</label>
                      <select class="form-control" name="fk_id_category">
                           <?php foreach ($categoryFaq as $data) { ?>
                              <option value="<?= $data->id_category ?>"><?= $data->name_category ?></option>
                          <?php } ?>
                      </select>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                  <table class="table table-bordered mb-0">
                    <thead>
                      <tr>
                        <th style="width: 2px;">No</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i = 1; $i <= 15 ; $i++) { ?>
                        <tr>
                          <td><?php echo $i ?></td>
                          <td><input class="form-control" type="file" name="img_faq[]"></td>
                          <td><input class="form-control" type="text" name="title[]" placeholder="Required Title ...."></td>
                          <td><input class="form-control" type="text" name="description[]" placeholder="Required Description ...."></td>
                        </tr>
                      <?php }?>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEditSection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?= base_url('action/Master/editSectionProcess')?>" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Section</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Category Content</label>
                      <select class="form-control" name="fk_id_category">
                           <?php foreach ($categoryFaq as $data) { ?>
                              <option value="<?= $data->id_category ?>"><?= $data->name_category ?></option>
                          <?php } ?>
                      </select>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                  <table class="table table-bordered mb-0">
                    <thead>
                      <tr>
                        <th style="width: 2px;">No</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i = 1; $i <= 15 ; $i++) { ?>
                        <tr>
                          <td><?php echo $i ?></td>
                          <td><input class="form-control" type="file" name="img_faq[]"></td>
                          <td><input class="form-control" type="text" name="title[]" placeholder="Required Title ...."></td>
                          <td><input class="form-control" type="text" name="description[]" placeholder="Required Description ...."></td>
                        </tr>
                      <?php }?>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
