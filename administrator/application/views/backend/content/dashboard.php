<div class="row">
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info" style="border-radius:20px;">
      <div class="inner">
        <h3><?= $totalcontent ?></h3>
        <p><b>Total Content</b></p>
      </div>
      <div class="icon">
        <i class="ion ion-person"></i>
      </div>
      <a href="./?data=x" class="small-box-footer" style="border-radius:20px;">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <!-- <div class="col-lg-3 col-6">
    <div class="small-box bg-success" style="border-radius:20px;">
      <div class="inner">
        <h3><?=  $totalSiswaXI ?></h3>
        <p><b>Total Siswa - XI</b></p>
      </div>
      <div class="icon">
        <i class="ion ion-person"></i>
      </div>
      <a href="./?data=xi" class="small-box-footer" style="border-radius:20px;">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-warning" style="border-radius:20px;">
      <div class="inner">
        <h3><?= $totalSiswaXII ?></h3>
        <p><b>Total Siswa - XII</b></p>
      </div>
      <div class="icon">
        <i class="ion ion-person"></i>
      </div>
      <a href="./?data=xii" class="small-box-footer" style="border-radius:20px;">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div> -->
  <!-- <div class="col-lg-3 col-6">
    <div class="small-box bg-danger" style="border-radius:20px;">
      <div class="inner">
        <h3><?= $totalSeluruhSiswa ?></h3>
        <p><b>TOTAL SELURUH SISWA</b></p>
      </div>
      <div class="icon">
        <i class="ion ion-person-stalker"></i>
      </div>
      <a href="#" class="small-box-footer" style="border-radius:20px;">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div> -->
</div>
</div>
