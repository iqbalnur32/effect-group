<div class="card card-primary">
  <div class="card-body">
    <form action="<?= base_url('action/Master/inputSectionHome')?>" method="post">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group ">
              <label class="control-label">Title Section</label>
              <input type="text" class="form-control" placeholder="Required Description ..." name="title_section" required autocomplete="off">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group ">
              <label class="control-label">Icons Image</label>
              <input type="text" class="form-control" placeholder="Required Icons Image ..." name="icons_image" required autocomplete="off">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group ">
              <label class="control-label">Image Section</label>
              <input type="text" class="form-control" placeholder="Required Image Section ..." name="image_section" required autocomplete="off">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group ">
              <label class="control-label">Description Section</label>
              <textarea class="form-control" placeholder="Required Description ..." name="description" required autocomplete="off"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="float-left">
            <button class="btn btn-xl btn-primary" type="submit">Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>