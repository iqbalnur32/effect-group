<div class="card">
  <div class="card-body">
    <form action="<?= base_url('action/Master/inputBaner')?>" method="post" enctype="multipart/form-data">
      <!-- <div class="row" id="file_gambar_banner">
        <div class="col-md-4 file_gmbr_banner">
          <div class="form-group">
            <label for="exampleInputFile">File Gambar</label> 
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" multiple="" name="img_banner[]" required id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
              <div class="input-group-append"> 
                  <button class="btn btn-sm btn-outline-secondary" type="button" id="add_file_gambar_banner"><i class="fas fa-plus"></i></button>
              </div>
            </div>
            <small style="color: red;">* Masukan Gambar Banner</small><br> 
            <small style="color: red">* Pastikan ukuran gambar harus 1920x800 untuk hasil gambar yang bagus</small>
          </div>
        </div>
      </div> -->
      <div class="row">
        <div class="col-md-12">
          <div class="form-group ">
            <label class="control-label">Nama Banner</label>
            <input class="form-control" placeholder="Banner Name" name="nama_banner" required autocomplete="off">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group ">
            <label class="control-label">Endpoint Url</label>
            <input class="form-control" placeholder="Required Endpoint Url" name="endpoint_url" autocomplete="off">
            <small style="color: red; font-size: 15px;">* Example: https://localhost/effect-club</small>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group ">
            <label class="control-label">Description</label>
            <textarea class="form-control" placeholder="Description Banner" name="description" required autocomplete="off"></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="float-left">
            <button type="submit" class="btn btn-info" name="Submit"><i class="fas fa-plus" style="font-size:15px;"></i> Tambahkan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
