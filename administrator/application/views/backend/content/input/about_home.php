<div class="card card-primary">
  <div class="card-body">
    <form action="<?= base_url('action/Master/inputAboutUsHome')?>" method="post">
      <div class="col-md-12">
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <label class="control-label">Title About</label>
                <input type="text" class="form-control" placeholder="Required Title ..." name="title_about" required autocomplete="off">
              </div>
            <div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <label class="control-label">Description About</label>
                <textarea class="form-control" placeholder="Required Description About ..." name="description" required autocomplete="off"> </textarea>
              </div>
            <div>
          </div>
          <div class="float-left">
            <button type="submit" class="btn btn-info" name="Submit"><i class="fas fa-plus" style="font-size:15px;"></i> Tambahkan</button>
          </div>
        </div>
      </div>
    </form> 
  </div>
</div>


