<div class="card-primary">
  <div class="card">
    <div class="card-body">
      <div class="container">
        <form action="<?= base_url('action/Master/inputContent') ?>" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Category</label>
                <select class="form-control" name="id_category" required=""> 
                  <option value="">Select Category : </option>
                  <?php 
                    foreach ($listcategory as $data) { ?>
                      <option value="<?= $data->id_category ?>"><?= $data->name_category ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Type Promo Product</label>
                <input class="form-control" name="type_product"  placeholder="Masukan seperti Flash Sale Dll" />
              </div>
            </div>
          </div>
          <div class="row" id="file_gambar">
            <div class="col-md-4 file_gmbr">
              <div class="form-group">
                <label for="exampleInputFile">File Gambar</label> 
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" multiple="" name="img_content[]" required id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                  <div class="input-group-append"> 
                      <button class="btn btn-sm btn-outline-secondary" type="button" id="add_file_gambar"><i class="fas fa-plus"></i></button>
                  </div>
                  <!-- <div class="input-group-append"> 
                      <button class="btn btn-outline-danger" id="remove_img-btn" type="button"><i class="fas fa-trash"></i></button>
                  </div> -->
                  <!-- <input type="file" class="form-control" multiple name="img_content[]" required> -->
                </div>
                <small style="color: red;">* Masukan Gambar Depan, Samping, Belakang</small><br> 
                <small style="color: red">* Pastikan ukuran gambar harus 1920x800 untuk hasil gambar yang bagus</small>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name="title" placeholder="Masukan Title Product">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label>Price</label>
                <input class="form-control" type="text" id="rupiah" name="price" placeholder="Masukan Range Harga produk ">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Link 1</label>
                <input class="form-control" type="text" name="link1" placeholder="Masukan Link Tokopedia">
                <small style="color: red">* Link Untuk Tokopedia</small>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label>Link 2</label>
                <input class="form-control" type="text" name="link2" placeholder="Masukan Link Shopee">
                <small style="color: red">* Link Untuk Shopee</small>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label>Stock Barang</label>
                <input class="form-control" type="number" name="stock" id="stock" placeholder="Masukan Stock Product">
              </div>
            </div>
          </div>
         <!--  <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label>Sisa Stock Barang</label>
                <input class="form-control" type="number" name="sisa_stock" id="sisa_stock" placeholder="Masukan Sisa Stock Product">
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                  <label>Description</label>
                  <textarea class="text-description form-control" name="description" id="description"></textarea>
                </div>
            </div>
          </div>
          <div>
            <button class="btn btn-sm btn-primary" type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    // Covert Format Rupiah  
    var rupiah = document.getElementById("rupiah");
    rupiah.addEventListener("keyup", function(e) {
        rupiah.value = formatRupiah(this.value, "Rp. ");
    });
    function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

</script>