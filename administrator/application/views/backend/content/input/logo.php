<div class="card card-primary">
  <div class="card-body">
    <form action="<?= base_url('action/Master/inputLogo')?>" method="post" enctype="multipart/form-data">
      <div class="col-md-12">
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputFile">File Logo</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="img_logo" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                </div>
                <small style="color: red">* Pastikan ukuran gambar harus 149x37 untuk hasil gambar yang bagus</small>
              </div>
            <button type="submit" class="btn btn-info" name="Submit"><i class="fas fa-plus" style="font-size:15px;"></i> Tambahkan</button>
            </div>
      </div>
    </form> 
  </div>
</div>


