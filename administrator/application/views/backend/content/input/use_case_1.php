<div class="card card-primary">
  <div class="card-body">
    <form action="<?= base_url('action/Master/inputCase1')?>" method="post">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group ">
              <label class="control-label">Title Case 1</label>
              <input type="text" class="form-control" placeholder="Required Title ..." name="title_case1" required autocomplete="off">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group ">
              <label class="control-label">Description Section</label>
              <textarea class="form-control" placeholder="Required Description ..." name="description" required autocomplete="off"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="float-left">
            <button class="btn btn-xl btn-primary" type="submit">Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>