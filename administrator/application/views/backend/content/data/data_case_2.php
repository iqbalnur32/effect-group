<div class="card-primary">
  <div class="card">
     <div class="card-body">
     	<div class="container">
     		<table class="table table-bordered">
     			<thead>
     				<tr>
     					<th>No</th>
     					<th>Title</th>
              <th>Description</th>
     					<th>Action</th>
     				</tr>
     			</thead>
     			<tbody>
     			<?php $i = 1; foreach ($dataCase2 as $key): ?>
     				<tr>
     					<td><?= $i++ ?></td>
     					<td><?= $key->title ?></td>
              <td><?= substr($key->description, 0, 15) . '...' ?></td>
     					<td>
     						<button id="modal_use_case_2" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal"
     						data-id_use_case_2="<?= $key->id_use_case_2 ?>"
							  data-title="<?= $key->title ?>"
                data-description="<?= $key->description ?>"
     						><i class="fas fa-pencil-alt"></i></button>				
     						<a href="<?= base_url('action/Master/deleteUseCase2/').$key->id_use_case_2 ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
     					</td>
     				</tr>
     			<?php endforeach ?>
     			</tbody>
     		</table>
     	</div>
     </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('action/Master/editUseCase2')?>" method="post">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label class="control-label">Title Case 1</label>
                  <input type="hidden" class="form-control" name="id_use_case_2" required id="id_use_case_2">
                  <input type="text" class="form-control" placeholder="Required Title ..." name="title_case2" required autocomplete="off" id="title">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label class="control-label">Description Section</label>
                  <textarea class="form-control" placeholder="Required Description ..." name="description" required autocomplete="off" id="description"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="float-left">
                <button class="btn btn-xl btn-primary" type="submit">Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


