<div class="card-primary">
  <div class="card">
     <div class="card-body">
     	<div class="container">
     		<table class="table table-bordered">
     			<thead>
     				<tr>
     					<th>No</th>
     					<th>Title</th>
              <th>Description</th>
     					<th>Action</th>
     				</tr>
     			</thead>
     			<tbody>
     			<?php $i = 1; foreach ($dataAboutUs as $key): ?>
     				<tr>
     					<td><?= $i++ ?></td>
     					<td><?= $key->title_about ?></td>
              <td><?= substr($key->description, 0, 15) . '...' ?></td>
     					<td>
     						<button id="modal_category" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal"
     						data-id_about="<?= $key->id_about ?>"
							  data-title_about="<?= $key->title_about ?>"
     						><i class="fas fa-pencil-alt"></i></button>				
     						<a href="<?= base_url('action/Master/deleteAboutUsHome/').$key->id_about ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
     					</td>
     				</tr>
     			<?php endforeach ?>
     			</tbody>
     		</table>
     	</div>
     </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('action/Master/editAboutUsHomePage') ?>" method="POST">
        	<div class="form-group">
        		<label>Category</label>
        		<input class="form-control" type="text" name="name_category" id="category">
        		<input class="form-control" type="hidden" name="id_category" id="id_category">
        	</div>
        	<div class="col-lg-12">
	    		<div class="float-left">
	    			<button class="btn btn-sm btn-warning" id="submit">Submit</button>
	    		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


