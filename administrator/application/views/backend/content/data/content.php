<div class="card-primary">
  <div class="card">
     <div class="card-body">
     		<table class="table table-bordered">
     			<thead>
     				<tr>
     					<th>No</th>
     					<th>Title</th>
                        <th>Gambar</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Link 1</th>
                        <th>Link 2</th>
     					<th>Action</th>
     				</tr>
     			</thead>
     			<tbody>
     			<?php $i = 1; foreach ($dataContentJoin as $key): ?>
     				<tr>
     					<td><?= $i++ ?></td>
              <td class="text-nowrap"><?= substr($key->title, 0, 12) .'...' ?></td> 
              <td>
                <div class="row">
                  <!-- nanti rubah yang tadinya double code jadi singgle coude -->
                    <?php foreach ((array) json_decode($key->img_content)as $picture) { ?>
                      <div class="col-xl-3 order-xl-2">
                        <img src="<?= base_url('assets/img/') . $picture ?>" style="height:50px; width:50px"/>
                      </div>
                    <?php } ?>
                  </div>
              </td>
              <td><?= substr($key->description,0,15) . '.....' ?></td>
              <td class="text-nowrap"><?= 'Rp '. number_format($key->price,0,",",".") ?></td>
              <td><?= $key->link_1 ?></td>
              <td><?= $key->link_2 ?></td>
     					<td class="d-flex">
     						<button id="modal_content" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal"
     						data-id_content="<?= $key->id_content ?>"
     						><i class="fas fa-pencil-alt"></i></button>				
     						<a href="<?= base_url('action/Master/deleteContentProcess/'.$key->id_content) ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
     					</td>
     				</tr>
     			<?php endforeach ?>
     			</tbody>
     		</table>
     </div>
  </div>
</div>

<!-- MODAL ADD ORDER -->
<div class="modal fade show" id="exampleModal" aria-modal="true" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('action/Master/editContentProcess') ?>" method="POST" enctype="multipart/form-data">
                  <input type="hidden" name="id_content" id="id_content">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="">Category</label>
                        <option value="">Select Category : </option>
                        <select class="form-control" name="id_category" id="id_category">
                          <?php 
                            foreach ($dataCategoryDB as $data) { ?>
                              <option value="<?= $data->id_category ?>"><?= $data->name_category ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="">Type Promo Product</label>
                        <input class="form-control" name="type_product" id="type_product"  placeholder="Masukan seperti Flash Sale Dll" />
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12" id="img">
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputFile">File Gambar</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" multiple="" name="img_content[]" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" type="text" name="title" id="title">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label>Price</label>
                        <input class="form-control" type="text" name="price" id="price">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label>Link 1</label>
                        <input class="form-control" type="text" name="link1" id="link_1">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label>Link 2</label>
                        <input class="form-control" type="text" name="link2" id="link_2">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label>Stock Barang</label>
                        <input class="form-control" type="text" name="stock" id="stock">
                      </div>
                    </div>
                  </div>
                  <!-- <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label>Sisa Stock Barang</label>
                        <input class="form-control" type="text" name="sisa_stock" id="sisa_stock">
                      </div>
                    </div>
                  </div> -->
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                          <label>Description</label>
                          <textarea class="form-control" name="description" id="description"></textarea>
                        </div>
                    </div>
                  </div>
                  <div>
                    <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                  </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<script type="text/javascript">

    // Covert Format Rupiah  
    var rupiah = document.getElementById("price");
    rupiah.addEventListener("keyup", function(e) {
        rupiah.value = formatRupiah(this.value, "Rp. ");
    });
    function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

</script>

