<div class="card-primary">
  <div class="card">
     <div class="card-body">
     	<div class="container">
     		<table class="table table-bordered">
     			<thead>
     				<tr>
     					<th>No</th>
     					<th>Nama Banner</th>
              <th>Gambar</th>
     					<th>Action</th>
     				</tr>
     			</thead>
     			<tbody>
     			<?php $i = 1; foreach ($dataBannerDB as $key): ?>
     				<tr>
     					<td><?= $i++ ?></td>
     					<td><?= $key->nama_banner ?></td>
              <td><?= substr($key->nama_banner, 0, 15) . '...' ?></td>
              <!-- <td>
                <div class="row">
                  <?php foreach ((array) json_decode($key->img_banner)as $picture) { ?>
                      <div class="col-xl-3 order-xl-2">
                        <img style="height:50px; width:50px" src="<?= base_url('assets/img/').$picture ?>">
                      </div>
                    <?php } ?>
                </div>
              </td> -->
     					<td>
     						<button id="banner_category" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal"
     						data-id_banner="<?= $key->id_banner ?>"
     						><i class="fas fa-pencil-alt"></i></button>				
     						<a href="<?= base_url('action/Master/deleteBannerProcess/'.$key->id_banner) ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
     					</td>
     				</tr>
     			<?php endforeach ?>
     			</tbody>
     		</table>
     	</div>
     </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="<?= base_url('action/Master/editBannerProcess') ?>" method="POST" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Master Data Banner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        	<div class="form-group">
        		<label>Nama Banner</label>
        		<input class="form-control" type="text" name="nama_banner" id="nama_banner">
        		<input class="form-control" type="hidden" name="id_banner" id="id_banner">
        	</div>
          <div class="form-group">
            <label>Description Banner</label>
            <textarea class="form-control" name="description" id="description_banner"></textarea>
          </div>
          <!-- <div class="form-group">
            <div class="col-md-12" id="img">
              
            </div>
          </div>
          <div class="form-group">
            <input class="form-control" type="file" name="img_banner" id="img_banner">
            <label for="exampleInputFile">File Gambar</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="img_banner[]" multiple="" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <small style="color: red">* Kamu Bisa Mengupload lebih dari 1 gambar banner max 3</small>
            <small style="color: red">* Pastikan ukuran Banner harus 1920x800 untuk hasil gambar yang bagus</small>
          </div> -->
        	<div class="col-lg-12">
	    		<div class="float-left">
	    			
	    		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning" type="submit">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </form>
    </div>
  </div>
</div>


