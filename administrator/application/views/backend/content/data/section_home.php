<div class="card-primary">
  <div class="card">
     <div class="card-body">
     	<div class="container">
     		<table class="table table-bordered">
     			<thead>
     				<tr>
     					<th>No</th>
     					<th>Title Home</th>
     					<th>Description Home</th>
     					<th>Action</th>
     				</tr>
     			</thead>
     			<tbody>
     			<?php $i = 1; foreach ($dataSectionHome as $key): ?>
     				<tr>
     					<td><?= $i++ ?></td>
     					<td><?= $key->title_home ?></td>
     					<td><?= $key->description_home ?></td>
     					<td>
     						<button id="edit_section_home" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal"
     						  data-id_home="<?= $key->id_home ?>"
							    data-title_home="<?= $key->title_home ?>"
							    data-description_home="<?= $key->description_home ?>"
							    data-icons_home="<?= $key->icons_home ?>"
                  data-image_home="<?= $key->image_home ?>"
     						><i class="fas fa-pencil-alt"></i></button>				
     						<a href="<?= base_url('action/Master/deleteSectionHomeProcess/').$key->id_home ?>" class="btn btn-sm btn-danger" onclick="return confirm(`Yakin Delete?`)"><i class="fas fa-trash-alt"></i></a>				
     					</td>
     				</tr>
     			<?php endforeach ?>
     			</tbody>
     		</table>
     	</div>
     </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="<?= base_url('action/Master/editSectionHomeProcess') ?>" method="POST">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <div class="row">
             <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group ">
                      <label class="control-label">Title Section</label>
                      <input type="hidden" class="form-control" name="id_home" required autocomplete="off" id="id_home">
                      <input type="text" class="form-control" placeholder="Required Description ..." name="title_section" required autocomplete="off" id="title_home">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group ">
                      <label class="control-label">Icons Image</label>
                      <input type="text" class="form-control" placeholder="Required Icons Image ..." name="icons_image" required autocomplete="off" id="icons_home">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group ">
                      <label class="control-label">Image Section</label>
                      <input type="text" class="form-control" placeholder="Required Image Section ..." name="image_section" required autocomplete="off" id="image_home">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group ">
                      <label class="control-label">Description Section</label>
                      <textarea class="form-control" placeholder="Required Description ..." name="description" required autocomplete="off" id="description_home"></textarea>
                    </div>
                  </div>
                </div>
              </div>
           </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>


