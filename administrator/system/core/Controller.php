<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

	/**
	 * CI_Loader
	 *
	 * @var	CI_Loader
	 */
	public $load;

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}
	public function debug($data,$withDie = null) {
		echo "<pre>";
		print_r($data);
		if ($withDie) {
			die;
		}
	}
	public function beautyDate($date,$withTime = null) {
		$response = substr($date, 8,2).' '.$this->genMonth(substr($date, 5,2)).' '.substr($date, 0,4);
		if ($withTime) {
			$response .= ' '.substr($date, 11,5);
		}
		return $response;
	}
	public function genMonth($value) {
		$callback = 'January';
		switch ($value) {
			case '01':
				$callback = 'January';
				break;
			case '02':
				$callback = 'February';
				break;
			case '03':
				$callback = 'Maret';
				break;
			case '04':
				$callback = 'April';
				break;
			case '05':
				$callback = 'Mei';
				break;
			case '06':
				$callback = 'Juni';
				break;
			case '07':
				$callback = 'July';
				break;
			case '08':
				$callback = 'Agustus';
				break;
			case '09':
				$callback = 'September';
				break;
			case '10':
				$callback = 'Oktober';
				break;
			case '11':
				$callback = 'November';
				break;
			case '12':
				$callback = 'Desember';
				break;
		}
		return $callback;
	}
	public function genDay($value) {
		$callback = 'Senin';
		switch (strtolower($value)) {
			case 'mon':
				$callback = 'Senin';
				break;
			case 'tue':
				$callback = 'Selasa';
				break;
			case 'wed':
				$callback = 'Rabu';
				break;
			case 'thu':
				$callback = 'Kamis';
				break;
			case 'fri':
				$callback = 'Jumat';
				break;
			case 'sat':
				$callback = 'Sabtu';
				break;
			case 'sun':
				$callback = 'Minggu';
				break;
		}
		return $callback;
	}
	public function genBeautyDate($data,$fields = []) {
		foreach ($data as $row => $value) {
			foreach ($fields as $r => $v) {
				if (isset($data[$row]->$v)) {
					$data[$row]->$v = $this->beautyDate($data[$row]->$v,true);
				}
			}
		}
		return $data;
	}
	public function genSqlDate($data,$fields,$toHuman = null) {
		foreach ($data as $row => $value) {
			foreach ($fields as $r => $v) {
				$data[$row]->$v = $this->toSqlDate($v,$toHuman);
			}
		}
		return $data;
	}
	public function toSqlDate($date,$toHuman = null) {
		$arr = explode('-', $date);
		if (count($arr) > 0) {
			if (strlen($arr[0]) == 4) {
				if ($toHuman) {
					return substr($date, 8,2).'-'.substr($date, 5,2).'-'.substr($date, 0,4);
				} else {
					return $date;
				}
			} else {
				return substr($date, 6,4).'-'.substr($date, 3,2).'-'.substr($date, 0,2);
			}
		} else {
			return $date;
		}
	}
	public function setLog($message = null) {
		return $this->db->insert('ms_logs',[
			'id_admin' => $this->session->userdata('LOGGED_ID'),
			'logs_desc' => $message,
			'logs_created' => date('Y-m-d H:i:s')
		]);
	}
}
