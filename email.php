<?php

require 'vendor/autoload.php';
use Mailgun\Mailgun;

$mg = Mailgun::create('key-27b5df334d78c60cdc2c33f37f3aeaad', 'https://api.eu.mailgun.net'); // For EU servers

// Now, compose and send your message.
// $mg->messages()->send($domain, $params);
$mg->messages()->send('dashboard.effect.group', [
  'from'    => 'support@effect.group',
  'to'      => 'alfyan@juara.io',
  'template' => 'contact_us',
  'subject' => 'The PHP SDK is awesome!'
]);

?>